/**
 * @file reader.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines methods and API for reading input data.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "input.h"

#include <exception>
#include <string>
#include <iostream>
#include <cstdio>
#include <cstdlib>

using esicup::common::InputData;

namespace esicup {
namespace io {

/**
 * @brief Reader is an entity to read the instance-related 
 * data to the InputData structure.
 * 
 */
class Reader {
public:
    /**
     * @brief Reads InputData from instance directory.
     * 
     * @param directory The directory with instance-related information.
     * @return The corresponding InputData.
     */
    InputData ReadData(const std::string& directory);

private:
    /**
     * @brief Reads information about bins.
     * 
     * @param inputData The data structure to read information into.
     * @param fileName The name of the file to read information from.
     */
    void ReadBinsFile(InputData& inputData, const std::string& fileName);
    /**
     * @brief Reads information about items.
     * 
     * @param inputData The data structure to read information into.
     * @param fileName The name of the file to read information from.
     */
    void ReadItemsFile(InputData& inputData, const std::string& fileName);
    /**
     * @brief Reads parameters and requirements.
     * 
     * @param inputData The data structure to read information into.
     * @param fileName The name of the file to read information from.
     */
    void ReadParametersFile(InputData& inputData, const std::string& fileName);

public:
    /// @brief The suffix representing the name of the file with bins.
    static const std::string binsFileSuffix;
    /// @brief The suffix representing the name of the file with items.
    static const std::string itemsFileSuffix;
    /// @brief The suffix representing the name of thee file with parameters.
    static const std::string parametersFileSuffix;

};

/**
 * @brief The method to read directory structure, i.e. the set of instance names
 * given the path to the root folder containing set of different instances inside.
 * 
 * @param pathToDirectory The path to the root directory.
 * 
 * @return The vector with instance names in the instance set.
 */
std::vector<std::string> InitialRead(std::string pathToDirectory);

} // namespace io
} // namespace esicup
