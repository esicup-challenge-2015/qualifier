/// Implements methods and API mentioned in reader.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "reader.h"
#include "input.h"

#include <sstream>
#include <fstream>
#include <string>
#include <stdexcept>

using esicup::common::Bin;
using esicup::common::Item;

namespace esicup {
namespace io {

std::vector<std::string> InitialRead(std::string pathToDirectory) {
  std::ifstream reader(pathToDirectory + "/list_instances.csv");
  std::string name;
  std::vector<std::string> answer;
  while (reader >> name) {
    answer.push_back(name);
  }
  return answer;
}

InputData Reader::ReadData(const std::string& directory) {
    InputData inputData;
    std::string binsFile = directory + "/" + binsFileSuffix;
    ReadBinsFile(inputData, binsFile);
    std::string itemsFile = directory + "/" + itemsFileSuffix;
    ReadItemsFile(inputData, itemsFile);
    std::string parametersFile = directory + "/" +  parametersFileSuffix;
    ReadParametersFile(inputData, parametersFile);
    inputData.BuildProductCount();
    inputData.itemCount = inputData.items.size();
    return inputData;
}

void Reader::ReadBinsFile(InputData& inputData, const std::string& fileName) {
  std::ifstream stream(fileName);
  if (!stream) {
    throw std::runtime_error("Cannot open bin file:  " + fileName);
  }
  unsigned binsAmount = 0;

  std::string line;
  for (std::getline(stream, line), std::getline(stream, line); stream; std::getline(stream, line)) {
    std::istringstream stream(line);
    std::string binType;
    std::getline(stream, binType, ';');

    std::string entry;
    std::getline(stream, entry, ';');
    unsigned length = std::stoi(entry);
    std::getline(stream, entry, ';');
    unsigned width = std::stoi(entry);
    std::getline(stream, entry, ';');
    unsigned height = std::stoi(entry);
    std::getline(stream, entry, ';');
    unsigned maximumWeightAllowed = std::stoi(entry);

    ++binsAmount;
    Bin bin;
    bin.binType = binType;
    bin.length = length;
    bin.width = width;
    bin.height = height;
    bin.maximumWeightAllowed = maximumWeightAllowed;

    inputData.bins.push_back(bin);
  }
}

void Reader::ReadItemsFile(InputData& inputData, const std::string& fileName) {
  std::ifstream stream(fileName);
  if (!stream) {
    throw std::runtime_error("Cannot open items file");
  }

  unsigned itemsAmount = 0;
  std::string line;
  for (std::getline(stream, line), std::getline(stream, line); stream; std::getline(stream, line)) {
    std::istringstream stream(line);
    std::string entry;

    std::getline(stream, entry, ';');
    unsigned itemId = std::stoi(entry);
    std::getline(stream, entry, ';');
    unsigned length = std::stoi(entry);
    std::getline(stream, entry, ';');
    unsigned width = std::stoi(entry);
    std::getline(stream, entry, ';');
    unsigned height = std::stoi(entry);
    std::getline(stream, entry, ';');
    double weight = common::ConvertToDouble(entry);

    std::getline(stream, entry, ';');
    common::Material material = common::StringToMaterial(entry);
    std::string constraints;
    std::getline(stream, constraints, ';');
    std::string productId;
    std::getline(stream, productId, ';');
    ++itemsAmount;

    Item item;
    item.itemId = itemId;
    item.length = length;
    item.width = width;
    item.height = height;
    item.weight = weight;
    item.material = material;
    item.FillConstraints(constraints);
    item.productId = productId;

    inputData.items.push_back(item);
  }
}

void Reader::ReadParametersFile(InputData& inputData, const std::string& fileName) {
  std::ifstream stream(fileName);
  if (!stream) {
    throw std::runtime_error("Cannot open parameters file: " + fileName);
  }
  std::vector<std::string> lines;
  while (stream) {
    std::string entry;
    stream >> entry;
    lines.push_back(entry);
  }
  inputData.context.FillContext(lines);
}

const std::string Reader::binsFileSuffix = "/input_bin.csv";
const std::string Reader::itemsFileSuffix = "/input_items.csv";
const std::string Reader::parametersFileSuffix = "/parameters.txt";

} // namespace io
} // namespace esicup
