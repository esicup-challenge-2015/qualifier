/**
 * @file input.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines the structures and API to work with the input information.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 * 
 */
#pragma once

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>


namespace esicup {
namespace common {

/**
 * @brief Converts the string to double.
 * 
 * @param strToConvert The string with typed double value.
 * 
 * @return The converted double value.
 */
double ConvertToDouble(const std::string& strToConvert);

/// @brief Enumeration for material type.
enum class Material {
    METAL,      ///< Metal
    CARDBOARD,  ///< Cardboard
    PLASTIC,    ///< Plastic
    WOOD,       ///< Wood
    UNDEFINED   ///< Undefined material
};

/// @brief Enumeration for input objects.
enum class ObjectType {
    ITEM,       ///< Item, to be packed
    BIN,        ///< Bin, type of trucks available for packing
    UNDEFINED   ///< Something else
};

/// @brief Enumeration for position. Sometimes specifies what allowed, 
/// sometimes what is assigned in packing.
enum class Position {
    WIDTHWISE,  ///< Widthwise position
    LENGTHWISE, ///< Lengthwise position
    BOTH,       ///< Both lengthwise and widthwise position allowed
    UNDEFINED   ///< Undefined position
};

/**
 * @brief Converts material enumeration to string representation.
 * 
 * @param material The material enumeration.
 * 
 * @return The string description of the material.
 */
std::string MaterialToString(Material material);

/**
 * @brief Converts string describing material to material enumeration.
 * 
 * @param str The string describing materials.
 * 
 * @return Corresponding material from Material enumeration.
 */
Material StringToMaterial(const std::string& str);

/**
 * @brief Context stores instance-specific parameters and requirements.
 */
struct Context {
public:
    /**
     * @brief Fills context from a string with parameters.
     * 
     * @param lines The vector with parameters for filling context.
     */
    void FillContext(const std::vector<std::string>& lines);
    /**
     * @brief Summarizes context requirements to a string.
     * 
     * @return A string describing the context.
     */
    std::string ToString() const;

public:
    /// @brief Specifies tolerance to item-product demands.
    double tolerance_item_product_demands;
    /// @brief Specified maximal allowed density in kg (for non-metal products).
    double maximum_density_allowed_kg;
    /// @brief Specifies maximal number of rows in a layer.
    unsigned max_number_of_rows_in_a_layer;
    /// @brief Specifies maximal number of items in a row.
    unsigned max_number_of_items_in_a_row;
    /// @brief Specifies the tolerance for layer size deviation.
    double layer_size_deviation_tolerance;
    /// @brief Specifies tolerance to row size deviation.
    double row_size_deviation_tolerance;
    /// @brief Specifies tolerance to item size deviation.
    double item_size_deviation_tolerance;
    /// @brief Specifies maximal weight above (no-metal) item in kg.
    double maximum_weigth_above_item_kg;
    /// @brief Maps product id to allowed count.
    std::map<std::string, unsigned> productCount;
};

/// @brief When size is undefined, set to zero.
const unsigned UNDEFINED_SIZE = 0;
/// @brief Empty objects have weight zero.
const double EMPTY_WEIGHT = 0.;
/// @brief Empty string constant for better code readability.
const std::string EMPTY_STRING = "";

/**
 * @brief Object defines a funding abstraction to entities provided at input.
 */
struct Object {
public:
    /**
     * @brief Constructor for a new object.
     * 
     * @param objectType The type of object.
     */
    Object(ObjectType objectType = ObjectType::UNDEFINED) : type(objectType) {}

    /**
     * @brief Virtual function to retrieve length. 
     * Returns undefined value if not overriden.
     * 
     * @return The length of the object.
     */
    virtual unsigned GetLength() const {
        return UNDEFINED_SIZE;
    }
    /**
     * @brief Virtual function to retrieve width. 
     * Returns undefined value if not overriden.
     * 
     * @return The width of the object.
     */
    virtual unsigned GetWidth() const {;
        return UNDEFINED_SIZE;
    }
    /**
     * @brief Virtual function to retrieve height. 
     * Returns undefined value if not overriden.
     * 
     * @return The height of the object.
     */
    virtual unsigned GetHeight() const {
        return UNDEFINED_SIZE;
    }
    /**
     * @brief Virtual function to retrieve description. 
     * Returns empty string if not overriden.
     * 
     * @return The description of the object.
     */
    virtual std::string GetName() const {
        return EMPTY_STRING;
    }
    /**
     * @brief Virtual function to retrieve weight. 
     * Returns zero weight if not overriden.
     * 
     * @return The weight of the object.
     */
    virtual double  GetWeight() const {
        return EMPTY_WEIGHT;
    }
    /**
     * @brief Virtual function to retrieve material. 
     * Returns undefined material if not overriden.
     * 
     * @return The material of the object.
     */
    virtual Material GetMaterial() const {
        return Material::UNDEFINED;
    }
    /**
     * @brief Virtual function to retrieve position. 
     * Returns undefined position if not overriden.
     * 
     * @return The position of the object.
     */
    virtual Position GetPosition() const {
        return Position::UNDEFINED;
    }
    /**
     * @brief Virtual destructor for proper polymorphism.
     */
    virtual ~Object() {}

public:
    /// @brief Specifies the type of object.
    ObjectType type;
};

/**
 * @brief Item represent an object that needs packing.
 */
struct Item : public Object {
public:
    /**
     * @brief Constructor. 
     */
    Item() : Object(ObjectType::ITEM) {}

    /**
     * @brief Retrieves item width.
     * 
     * @return The width of the item.
     */
    virtual unsigned GetWidth() const {
        return width;
    }
    /**
     * @brief Retrieves item length.
     * 
     * @return The length of the item.
     */
    virtual unsigned GetLength() const {
        return length;
    }
    /**
     * @brief Retrieves item height.
     * 
     * @return The height of the item.
     */
    virtual unsigned GetHeight() const {
        return height;
    }
    /**
     * @brief Retrieves item weight.
     * 
     * @return The weight of the item.
     */
    virtual double GetWeight() const {
        return weight;
    }
    /**
     * @brief Retrieves item material.
     * 
     * @return The material of the item.
     */
    virtual Material GetMaterial() const {
        return material;
    }
    /**
     * @brief Retrieves item description.
     * More precisely, product identifier.
     * 
     * @return The description of the item.
     */
    virtual std::string GetName() const {
        return productId;
    }
    /**
     * @brief Retrieves item position requirement.
     * 
     * @return The position of the item.
     */
    virtual Position GetPosition() const {
        if (canBeWidthwise && canBeLengthwise) {
            return Position::BOTH;
        } else if (canBeLengthwise) {
            return Position::LENGTHWISE;
        } else if (canBeWidthwise) {
            return Position::WIDTHWISE;
        } else {
            return Position::UNDEFINED;
        }
    }
    /**
     * @brief Fills position requirements based on 
     *      the textual description.
     * 
     * @param constraints Textual description of 
     *      the positional requirements.
     */
    void FillConstraints(std::string constraints) {
        canBeLengthwise = true;
        canBeWidthwise = true;
        if (constraints == "LENGTHWISE") {
            canBeWidthwise = false;
        } 
        if (constraints == "WIDTHWISE") {
            canBeLengthwise = false;
        }
    }
    /**
     * @brief Creates textual description of the item.
     * 
     * @return String with item description.
     */
    std::string ToString() const {
        std::string exportString;
        exportString += "Item: ";
        exportString += "\n\tId: ";
        exportString += std::to_string(itemId);
        exportString += "\n\tLength: ";
        exportString += std::to_string(length);
        exportString += "\n\tWidth: ";
        exportString += std::to_string(width);
        exportString += "\n\tHeight: ";
        exportString += std::to_string(height);
        exportString += "\n\tWeight: ";
        exportString += std::to_string(weight);
        if (canBeLengthwise) {
            exportString += "\n\tCan be lengthwise";
        }
        if (canBeWidthwise) {
            exportString += "\n\tCan be widthwise";
        }
        exportString += "\n\tMaterial: ";
        exportString += MaterialToString(material);
        exportString += "\n\tProductID: ";
        exportString += productId;
        exportString += "\n";
        return exportString;
    }

public:
    /// @brief Item identifier.
    unsigned itemId;
    /// @brief Item length.
    unsigned length;
    /// @brief Item width.
    unsigned width;
    /// @brief Item height.
    unsigned height;
    /// @brief Item weight.
    double weight;
    /// @brief Item material.
    Material material;
    /// @brief If item can get positioned lengthwise.
    bool canBeLengthwise;
    /// @brief If item can get positioned widthwise.
    bool canBeWidthwise;
    /// @brief The identifier of the item product.
    std::string productId;
};

/**
 * @brief Bin represents the truck that may be used to pack items.
 */
struct Bin : public Object {
public:
    /**
     * @brief Bin Constructor.
     */
    Bin() : Object(ObjectType::BIN) {}
    /**
     * @brief Retrieves bin length.
     * 
     * @return The length of the bin.
     */
    virtual unsigned GetLength() const {
        return length;
    }
    /**
     * @brief Retrieves bin width.
     * 
     * @return The width of the bin.
     */
    virtual unsigned GetWidth() const {;
        return width;
    }
    /**
     * @brief Retrieves bin height.
     * 
     * @return The height of the bin.
     */
    virtual unsigned GetHeight() const {
        return height;
    }
    /**
     * @brief Retrieves bin name.
     * More precisely, bin type.
     * 
     * @return The name of the bin. 
     */
    virtual std::string GetName() const {
        return binType;
    }
    /**
     * @brief Retrieves the maximal allowed weight in the bin.
     * 
     * @return The value of the maximal allowed weight in the bin.
     */
    virtual double GetWeight() const {
        return maximumWeightAllowed;
    }

    /**
     * @brief Creates textual representation of the bin.
     * 
     * @return String with textual representation.
     */
    std::string ToString() const {
        std::string exportString;
        exportString += "Bin: ";
        exportString += "\n\tType: ";
        exportString += binType;
        exportString += "\n\tLength: ";
        exportString += std::to_string(length);
        exportString += "\n\tWidth: ";
        exportString += std::to_string(width);
        exportString += "\n\tHeight: ";
        exportString += std::to_string(height);
        exportString += "\n\tMaximum weight: ";
        exportString += std::to_string(maximumWeightAllowed);
        exportString += "\n";
        return exportString;
    }

public:
    /// @brief The length of the bin category.
    unsigned length;
    /// @brief The width of the bin category.
    unsigned width;
    /// @brief The height of the bin category.
    unsigned height;
    /// @brief The maximal weight allowed in the bin category.
    unsigned maximumWeightAllowed;
    /// @brief The name of the bin category.
    std::string binType;
};

/**
 * @brief InputData is a summary of the information provided at input
 * for a particular instance. Briefly, set of items, truck opportunities,
 * the instance-specific parameters and requirements.
 */
struct InputData {
public:
    /**
     * @brief Builds the structure storing product counts.
     */
    void BuildProductCount();

public:
    /// @brief The number of the items.
    unsigned itemCount;
    /// @brief The context with instance-related parameters and requirements.
    Context context;
    /// @brief Maps product name to the number of the products.
    std::map<std::string, unsigned> productCount;
    /// @brief Vector with available bins representing trucks to pack items into.
    std::vector<Bin> bins;
    /// @brief Vector with available items for packing.
    std::vector<Item> items;
};

} // namespace common
} // namespace esicup

