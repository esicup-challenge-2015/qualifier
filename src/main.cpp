/// Defines entry point and calls corresponding functions.
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "common.h"

/**
 * @brief Entry point. Expected application:
 * ./<executable> -p set_of_instances_directory,
 * where set_of_instances_directory should
 * contain the set of instances to find a solution at.
 *
 * @param argc The number of arguments
 * @param argv The array with arguments.
 *
 */
int main(int argc, char** argv) {
    auto inputParameters = ParseInputFromCLI(argc, argv);
    if (!inputParameters.initOk) {
        std::cerr << "Bad input provided!\nPlease use format:\n\
            executable -p set_of_instances_directory\n";
        return 0;
    }
    try {
        ProcessInstances(inputParameters);
    } catch (const char* message) {
        std::cerr << "Error occured (" << message << ")" << std::endl;
        return 1;
    } catch (const std::exception& exception) {
        std::cerr << "Error occured (" << exception.what() << ")" << std::endl;
        return 1;
    } catch (...) {
        std::cerr << "Unknown error occured" << std::endl;
        return 1;
    }
    return 0;
}

