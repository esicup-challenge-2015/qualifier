/**
 * @file output.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines and implements structures and API 
 * related to the objects representing the completed packing.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 * 
 */
#pragma once

#include "input.h"
#include "geometry.h"

namespace esicup {
namespace common {

using common::Context;
using common::Position;
using common::InputData;
using geometry::Point3D;


/**
 * @brief Funding class for the objects storing output packing.
 */
class Foundation {
public:
    /**
     * @brief Constructor. 
     */
    Foundation(): id(0), idParent(0), weight(0) {}
    /**
     * @brief Sets the weight of the object.
     * 
     * @param weightValue The value of the weight.
     */
    void SetWeight(double weightValue){
        weight = weightValue;
    }
    /**
     * @brief Retrieves the weight of the object.
     * 
     * @return double The value of the weight.
     */
    double GetWeight() const {
        return weight;
    }
    /**
     * @brief Registers an index of a child.
     * 
     * @param idChild The index of the child to register.
     */
    void AddChild(unsigned idChild){
        childrenId.push_back(idChild);
    }
    /**
     * @brief Counts the number of children.
     * 
     * @return The number of the child objects.
     */
    unsigned GetCountChildren() const {
        return childrenId.size();
    }
    /**
     * @brief Provides identifier of the child object by index.
     * 
     * @param index The index in the vector childrenId.
     * @return The identifier of the child object in the global index.
     */
    unsigned GetChildIdByIndex(unsigned index) const {
        return childrenId[index];
    }
    /**
     * @brief Virtual destructor for proper polymorphism.
     */
    virtual ~Foundation() {}

public:
    /// @brief Identifier of the object.
    unsigned id;
    /// @brief Identifier of the parent object, if relevant.
    unsigned idParent;
    /// @brief The origin location coordinates.
    Point3D origin;
    /// @brief The extremity location coordinates.
    Point3D extremity;
    /// @brief The vector with child object indices.
    std::vector<unsigned> childrenId;
    /// @brief The weight of the object.
    double weight;
};

/**
 * @brief Represents filled bin, ready for checking and output.
 */
class OutputBin: public Foundation {
public:
    /**
     * @brief Sets the value of the maximal allowed weight.
     * 
     * @param weightValue The value of the maximal allowed weight.
     */
    void SetMaximumWeightAllowed(double weightValue) {
        maximumWeightAllowed = weightValue;
    }
    /**
     * @brief Gets the value of the maximal allowed weight.
     * 
     * @return The value of the maximal allowed weight.
     */
    double GetMaximumWeightAllowed() const {
        return maximumWeightAllowed;
    }

public:
    /// @brief The type of the bin.
    std::string type;

private:
    /// @brief The maximal allowed weight of the bin.
    double maximumWeightAllowed;
};

/**
 * @brief Represents filled stack, ready for checking and output.
 */
class OutputStack: public Foundation {
public:
    /**
     * @brief Stack constructor.
     */
    OutputStack() : isMetal(false) {}
    /**
     * @brief Set the boolean property isMetal.
     * 
     * @param value The value of the boolean property to set.
     */
    void SetIsMetal(bool value){
        isMetal = value;
    }
    /**
     * @brief Retrieve whether the stack consists of metal objects.
     * 
     * @return True if stack consists of metal objects, and False otherwise.
     */
    bool GetIsMetal() const {
        return isMetal;
    }

private:
    /// @brief Boolean property whether the stack consists of metal objects.
    bool isMetal;
};

/**
 * @brief Represents the filled layer, ready
 * for checking and output.
 */
class OutputLayer: public Foundation {
public:
    /**
     * @brief Constructor for Output Layer.
     */
    OutputLayer() :  isTop(false), isMetal(false) {}
    /**
     * @brief Sets the isMetal boolean property.
     * 
     * @param value The value of the boolean property.
     */
    void SetIsMetal(bool value){
        isMetal = value;
    }
    /**
     * @brief Gets the isMetal boolean property.
     * 
     * @return True if layer consists of metal objects, and False otherwise.
     */
    bool GetIsMetal() const {
        return isMetal;
    }
    /**
     * @brief Sets the isTop boolean property.
     * 
     * @param flag Whether the layer is the top layer.
     */
    void SetIsTop(bool flag) {
        isTop = flag;
    }
    /**
     * @brief Retrieves whether the layer is the top layer.
     * 
     * @return True if the layer is the top layer, and False otherwise.
     */
    bool GetIsTop() const {
        return isTop;
    }
private:
	/// @brief Boolean property whether the layer is top layer.
	bool isTop;
	/// @brief Boolean property whether the layer consists of metal objects.
	bool isMetal;
};

/**
 * @brief Represents the filled row, ready
 * for checking and output.
 */
class OutputRow: public Foundation {
public:
    /**
     * @brief Constructor for OutputRow object.
     */
    OutputRow() : isMetal(false) {}
    /**
     * @brief Sets the isMetal boolean property.
     * 
     * @param value The value of the boolean property.
     */
    void SetIsMetal(bool value) {
        isMetal = value;
    }
    /**
     * @brief Gets the isMetal boolean property.
     * 
     * @return True if row consists of metal objects, and False otherwise.
     */
    bool GetIsMetal() const {
        return isMetal;
    }
private:
	/// @brief Boolean property whether the row consists of metal objects.
    bool isMetal;
};

/**
 * @brief Represents the allocated item, ready for checking and output.
 * 
 */
class OutputItem: public Foundation {
public:
    /**
     * @brief Constructor for OutputItem object.
     */
    OutputItem() : isMetal(false) {}
    /**
     * @brief Sets the isMetal boolean property.
     * 
     * @param value The value of the boolean property.
     */
    void SetIsMetal(bool metal){
        isMetal = metal;
    }
    /**
     * @brief Gets the isMetal boolean property.
     * 
     * @return True if item consists of metal objects, and False otherwise.
     */
    bool GetIsMetal() const {
        return isMetal;
    }
    /**
     * @brief Set the item position.
     * 
     * @param value The value of the position (lengthwise, widthwise or both).
     */
    void SetPosition(Position value) {
        position = value;
    }
    /**
     * @brief Retrieves the item position.
     * 
     * @return The value of the position (lengthwise or widthwise).
     */
    Position GetPosition() const {
        return position;
    }
    /**
     * @brief Set the product id.
     * 
     * @param pIdValue The value of the product id.
     */
    void SetProductId(std::string pIdValue) {
        productId = pIdValue;
    }
    /**
     * @brief Retrieve the product id.
     * 
     * @return The value of the product id.
     */
    std::string GetProductId() const {
        return productId;
    }

private:
    /// @brief Boolean property whether the item consists of metal objects.
    bool isMetal;
    /// @brief The position of the item (lengthwise or widthwise).
    Position position;
    /// @brief The id of the product the item product.
    std::string productId;
};

/**
 * @brief Structure containing all output-related information.
 */
struct OutputData{
public:
    /// @brief The vector with filled bins.
    std::vector<OutputBin> bins;
    /// @brief The vector with filled stacks.
    std::vector<OutputStack> stacks;
    /// @brief The vector with filled layers.
    std::vector<OutputLayer> layers;
    /// @brief The vector with filled rows.
    std::vector<OutputRow> rows;
    /// @brief The vector with filled items.
    std::vector<OutputItem> items;
    /// @brief The context with problem parameters and requirements.
    Context context;
};

/**
 * @brief Completes the Output Data object provided by 
 * algorithm module by propagating corresponding values
 * and looking into the Input Data.
 */
class OutputCompleter{
public:
    /**
     * @brief Constructor. Stores input information.
     * 
     * @param inputData The InputData structure with input information.
     */
    OutputCompleter(const InputData& inputData);
    /**
     * @brief Completes the OutputData structure from the information
     * brought by algorithm module.
     * 
     * @param outputData The OutputData to complete.
     */
    void Fill(OutputData& outputData);

private:
    /**
     * @brief Fills the relevant fields for tiems inside OutputData.
     * 
     * @param outputData The OutputData to complete.
     */
    void FillItems(OutputData& outputData);
    /**
     * @brief Fills the relevant fields for rows inside OutputData.
     * 
     * @param outputData The OutputData to complete.
     */
    void FillRows(OutputData& outputData);
    /**
     * @brief Fills the relevant fields for layers inside OutputData.
     * 
     * @param outputData The OutputData to complete.
     */
    void FillLayers(OutputData& outputData);
    /**
     * @brief Fills the relevant fields for stacks inside OutputData.
     * 
     * @param outputData The OutputData to complete.
     */
    void FillStacks(OutputData& outputData);
    /**
     * @brief Fills the relevant fields for bins inside OutputData.
     * 
     * @param outputData The OutputData to complete.
     */
    void FillBins(OutputData& outputData);

private:
    /// @brief The reference to the instance input information.
    const InputData& inputData;
};

}
}
