/// Implements API defined in output.h.
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian Statu University, 2015
/// (c) Smith School of Business, 2024

#include "output.h"

namespace esicup {
namespace common {

using common::Material;

OutputCompleter::OutputCompleter(const InputData& inputInit) : inputData(inputInit) {}

void OutputCompleter::Fill(OutputData& outputData) {
    FillItems(outputData);
    FillRows(outputData);
    FillLayers(outputData);
    FillStacks(outputData);
    FillBins(outputData);
}

// and fill children for rows
void OutputCompleter::FillItems(OutputData& outputData) {
    for (unsigned idx = 0; idx < inputData.items.size(); ++idx){
        if (inputData.items[idx].material == Material::METAL) {
            outputData.items[idx].SetIsMetal(true);
        } else {
            outputData.items[idx].SetIsMetal(false);
        }

        outputData.items[idx].SetPosition(inputData.items[idx].GetPosition());
        outputData.items[idx].SetProductId(inputData.items[idx].productId);
        outputData.items[idx].SetWeight(inputData.items[idx].GetWeight());

        outputData.rows[outputData.items[idx].idParent].AddChild(idx);
    }
}

// and fill children for layers
void OutputCompleter::FillRows(OutputData& outputData) {
    for (unsigned idx = 0; idx < outputData.rows.size(); ++idx) {

        bool metal = false;
        double weight = 0;

        for (unsigned jnd = 0; jnd < outputData.rows[idx].GetCountChildren(); ++jnd){
            if (outputData.items[outputData.rows[idx].GetChildIdByIndex(jnd)].GetIsMetal()) {
                metal = true;
            }
            weight += outputData.items[outputData.rows[idx].GetChildIdByIndex(jnd)].GetWeight();
        }

        outputData.rows[idx].SetIsMetal(metal);
        outputData.rows[idx].SetWeight(weight);

        outputData.layers[outputData.rows[idx].idParent].AddChild(idx);
    }
}

// and fill children for stacks
void OutputCompleter::FillLayers(OutputData& outputData) {
    for (unsigned idx = 0; idx < outputData.layers.size(); ++idx) {
        bool metal = false;
        double weight = 0;

        for (unsigned jnd = 0; jnd < outputData.layers[idx].GetCountChildren(); ++jnd) {
            if (outputData.rows[outputData.layers[idx].GetChildIdByIndex(jnd)].GetIsMetal()) {
                metal = true;
            }
            weight += outputData.rows[outputData.layers[idx].GetChildIdByIndex(jnd)].GetWeight();
        }

        outputData.layers[idx].SetIsMetal(metal);
        outputData.layers[idx].SetWeight(weight);
        outputData.layers[idx].SetIsTop(false);
        outputData.stacks[outputData.layers[idx].idParent].AddChild(idx);
    }
}

// and fill children for bins and top layers
void OutputCompleter::FillStacks(OutputData& outputData) {
    for (unsigned idx = 0; idx < outputData.stacks.size(); ++idx) {

        bool metal = false;
        double weight = 0;

        unsigned idTopLayer = 0;
        unsigned maxHeight = 0;

        for (unsigned jnd = 0; jnd < outputData.stacks[idx].GetCountChildren(); ++jnd) {
            const OutputLayer& currentLayer = outputData.layers[
                outputData.stacks[idx].GetChildIdByIndex(jnd)];
            if (maxHeight < currentLayer.extremity.z) {
                maxHeight = currentLayer.extremity.z;
                idTopLayer = outputData.stacks[idx].GetChildIdByIndex(jnd);
            }

            if (currentLayer.GetIsMetal()) {
                metal = true;
            }
            weight += currentLayer.GetWeight();
        }

        outputData.layers[idTopLayer].SetIsTop(true);
        outputData.stacks[idx].SetIsMetal(metal);
        outputData.stacks[idx].SetWeight(weight);

        outputData.bins[outputData.stacks[idx].idParent].AddChild(idx);
    }
}

void OutputCompleter::FillBins(OutputData& outputData) {
    for (unsigned idx = 0; idx < outputData.bins.size(); ++idx) {

        double weight = 0;
        for (unsigned jnd = 0; jnd < outputData.bins[idx].GetCountChildren(); ++jnd) {
            weight += outputData.stacks[outputData.bins[idx].GetChildIdByIndex(jnd)].GetWeight();
        }
        outputData.bins[idx].SetWeight(weight);

        for (unsigned jnd = 0; jnd < inputData.bins.size(); ++jnd) {
            if (inputData.bins[jnd].binType == outputData.bins[idx].type) {
                outputData.bins[idx].SetMaximumWeightAllowed(inputData.bins[jnd].maximumWeightAllowed);
            }
        }
    }
}

}
}

