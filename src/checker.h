/**
 * @file checker.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines and implements API related to checking if the solution
 * satisfies the problem requirements.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 * 
 */
#pragma once

#include "input.h"
#include "output.h"

#include <string>
#include <map>


namespace esicup {
namespace checker {

using common::Context;
using common::InputData;
using common::ObjectType;
using common::OutputBin;
using common::OutputStack;
using common::OutputLayer;
using common::OutputRow;
using common::OutputItem;
using common::OutputData;
using common::OutputCompleter;
using common::Position;
using geometry::Point3D;

/**
 * @brief Computes volume of the orthogonal space 
 * segment between origin and extremity.
 * 
 * @param origin The point with lower coordinate values.
 * @param extremity The point with upper coordinate values.
 * 
 * @return The subspace volume.
 */
double GetVolume(Point3D origin, Point3D extremity);


/**
 * @brief Logger is a class to simplify aggregation
 * and output of the information when requirements 
 * are not satisfied.
 */
class Logger {
public:
    /**
     * @brief Adds message to logger.
     * 
     * @param id Identifier of the message.
     * @param message The message to store.
     */
    void AddToLog(unsigned id, std::string message) {
        registeredStatements.push_back(std::make_pair(id, message));
    }
    /**
     * @brief Getter for the object storing the log statements.
     * 
     * @return The log storage.
     */
    std::vector<std::pair<unsigned, std::string>> GetLogs() {
        return registeredStatements;
    }
    /**
     * @brief Writes log messages to standard error stream.
     */
    void ShowLogs() {
        for (const auto& entry : registeredStatements) {
            std::cerr << "id : " << entry.first << ", message : " << entry.second << std::endl;
        }
    }

private:
    /// @brief The log storage, pairs with identifiers and messages.
    std::vector<std::pair<unsigned, std::string>> registeredStatements;
};

/**
 * @brief Global Checker is the high-level checker that is responsible
 * for verifying solution feasiblity.
 */
class GlobalChecker {
public:
    /**
     * @brief Constructor for Global Checker.
     * 
     * @param outputData The solution to check.
     * @param inputData  The structure with instance inputs.
     */
    GlobalChecker(const OutputData& outputData, const InputData& inputData);
    /**
     * @brief Entry point towards complete checking.
     * 
     * @return True if the result is feasible, False otherwise.
     */
    bool CheckFeasibility();

private:
    /**
     * @brief Checks if every bin in the solution is not empty.
     * 
     * @param logger The logger to track errors if any.
     * @return True if bins are not empty, False otherwise.
     */
    bool IfEveryBinIsNotEmpty(Logger& logger);
    /**
     * @brief Checks if every stack in the solution is not empty.
     * 
     * @param logger The logger to track errors if any.
     * @return True if all stacks are not empty fine, False otherwise.
     */
    bool IfEveryStackIsNotEmpty(Logger& logger);
    /**
     * @brief Checks if every layer in the solution is not empty.
     * 
     * @param logger The logger to track errors if any.
     * @return True if all layers are not empty, False otherwise. 
     */
    bool IfEveryLayerIsNotEmpty(Logger& logger);
    /**
     * @brief Checks if every row in the solution is not emtpy.
     * 
     * @param logger The logger to track errors if any.
     * @return True if all rows are not empty, False otherwise.
     */
    bool IfEveryRowIsNotEmpty(Logger& logger);
    /**
     * @brief Checks if every item is packed. 
     * Constraint C5 in the problem description.
     * 
     * @return True if every item is packed, False otherwise. 
     */
    bool IfEachItemIsPacked();
    /**
     * @brief Checks if every id is applied correctly.
     * 
     * @tparam Type Type of object in the vector.
     * @param packages The vector with objects to verify.
     * @return True if every id is assigned correctly, and False otherwise.
     */
    template<typename Type>
    bool IfEveryIdAppliedCorrectly(const std::vector<Type>& packages);
    /**
     * @brief Checks if null bin has the smallest volume.
     * Bin 0 is a bin with id = 0. Accounts for constraints C7 in 
     * the problem description.
     * 
     * @return True if the requirement is satisfied, False otherwise. 
     */
    bool IfNullBinHasSmallestVolume(); 
    /**
     * @brief Pre-computes and retrieves the dictionary that maps
     * product id to the count in the bin zero.
     * 
     * @return The described above dictionary. 
     */
    std::map<std::string, unsigned> GetCountProductInBinZero();
    /**
     * @brief Checks if product counts in bin zero satisfy the
     * requirements.
     * 
     * @return True if the requirements are satisfied, and False otherwise.
     */
    bool CheckCountProductInBinZero();
    /**
     * @brief Checks every bin iteratively.
     * 
     * @param logger Logger to report errors if any.
     * @return True if all bins are checked successfully, and False otherwise.
     */
    bool CheckBins(Logger& logger);
    /**
     * @brief Checks every stack iteratively.
     * 
     * @param logger Logger to report errors if any.
     * @return True if all stacks are checked successfully, and False otherwise.
     */
    bool CheckStacks(Logger& logger);
    /**
     * @brief Checks every layer iteratively.
     * 
     * @param logger Logger to report errors if any.
     * @return True if all layers are checked successfully, and False otherwise.
     */
    bool CheckLayers(Logger& logger);
    /**
     * @brief Checks every row iteratively.
     * 
     * @param logger Logger to report errors if any.
     * @return True if all rows are checked successfully, and False otherwise.
     */
    bool CheckRows(Logger& logger);

private:
    /// @brief The reference to the solution that is checked.
    const OutputData& outputData;
    /// @brief The reference to the structure describing input.
    const InputData& inputData;
};

/**
 * @brief Checker that is responsible to check each particular bin.
 */
class BinChecker{
public:
    /**
     * @brief Constructs BinChecker.
     * 
     * @param bin The funding bin.
     * @param stacksVector The vector with stacks.
     */
    BinChecker(const OutputBin& bin, const std::vector<OutputStack>& stacksVector);
    /**
     * @brief Checks that the bin has not problems with weight.
     * Essentially, constraint C2 from the problem description.
     * 
     * @return True if check is fine, False otherwise.
     */
    bool CheckWeight();			
    /**
     * @brief Checks if stacks have no overlaps.
     * Essentially, constraint C3 from the problem description.
     * 
     * @return True if stacks have no overlaps, and False otherwise.
     */
    bool IfStacksNonOverlap();
    /**
     * @brief Checks if all stacks are located entirely inside the bin.
     * Essentially, constraints C4 and C9 from the problem setting.
     * 
     * @return True if all stacks are inside, and False otherwise.
     */
    bool IfStacksLayEntirelyInside();

private:
    /// @brief The reference to the checked bin.
    const OutputBin& outputBin;
    /// @brief The reference to the vector storing all stacks.
    const std::vector<OutputStack>& outputStacks;
};

/**
 * @brief Checker that is responsible to check each particular stack.
 */
class StackChecker{
public:
    /**
     * @brief Constructor for StackChecker.
     * 
     * @param stack The solution stack to check.
     * @param layersVector The vector with solution layers.
     * @param rowsVector  The vector with solution rows.
     * @param itemsVector The vector with solution items.
     * @param context The context with requirements and parameters.
     */
    StackChecker(const OutputStack& stack,
        const std::vector<OutputLayer>& layersVector,
        const std::vector<OutputRow>& rowsVector,
        const std::vector<OutputItem>& itemsVector,
        const Context& context);
    /**
     * @brief Checks if stack is built correctly. Essentially,
     * constraints C8, C9 and C11 from the problem setting.
     * 
     * @return True if stack is built correctly, and False otherwise.
     */
    bool IfStackIsBuiltCorrectly();
    /**
     * @brief Checks if layer dimensions are equal. 
     * Essentially, constraint C10 from the problem setting.
     * 
     * @return True if requirement is satisfied, and False otherwise.
     */
    bool IfLayersDimensionsAreEqual();
    /**
     * @brief Checks if density requirement is satisfied.
     * Essentially, condition C13 from the problem statement.
     * 
     * @return True if requirement is satisfied, and False otherwise.
     */
    bool CheckDensity();
    /**
     * @brief Checks if layers are sorted by weights.
     * Essentially, condition C14 from the problem statement.
     * 
     * @return True if requirement is satisfied, and False otherwise.
     */
    bool IfLayersAreSortedByWeights();
    /**
     * @brief Check whether the weight condition for the funding layer holds.
     * More precisely, condition C29 from problem description.
     * 
     * @return  True if requirement is satisfied, and False otherwise.
     */
    bool CheckWeightOnFundingLayerItems(); 
    /**
     * @brief Computes and returns areas for items in the bottom layer.
     * 
     * @return The vector with described areas.
     */
    std::vector<unsigned> GetAreasForItemsInBottomLayer();
    /**
     * @brief Checks metal packages. Essentially, requirement C12.
     * 
     * @return True if the requirement is satisfied, and False otherwise.
     */
    bool CheckMetalPackages();

private:
    /// @brief The reference to the solution stack to check.
    const OutputStack& outputStack;
    /// @brief The reference to the vector with solution layers.
    const std::vector<OutputLayer>& outputLayers;
    /// @brief The reference to the vector with solution rows.
    const std::vector<OutputRow>& outputRows;
    /// @brief The reference to the vector with solution items.
    const std::vector<OutputItem>& outputItems;
    /// @brief The reference to the context with parameters and requirements.
    const Context& context;
    /// @brief The vector with children indices sorted by decreasing weight.
    std::vector<unsigned> orderedChildrenIds;
};

/**
 * @brief Checker responsible for checking each particular layer.
 */
class LayerChecker{
public:
    /**
     * @brief Constructor for Layer Checker.
     * 
     * @param layer The solution layer for verification.
     * @param rowsVector The vector with solution rows.
     * @param context  The context with parameters and requirements.
     */
    LayerChecker(
        const OutputLayer& layer, 
        const std::vector<OutputRow>& rowsVector, 
        const Context& context);
    /**
     * @brief Checks for the maximal amount of rows allowed.
     * Essentially, constraint C16 from the statement.
     * 
     * @return True if constraints are satisfied, and False otherwise.
     */
    bool CheckMaxRowsAllowed();
    /**
     * @brief Checks if rows dimensions are equal.
     * Essentially, constraint C17 from the statement.
     * 
     * @return True if constraints are satisfied, and False otherwise.
     */
    bool IfRowsDimensionsAreEqual();
    /**
     * @brief Checks if all rows have the same height.
     * Essentially, condition C18 from the description.
     * 
     * @return True if the requirements are satisfied, and False otherwise.
     */
    bool IfAllRowsHaveSameHeight();	
    /**
     * @brief Checks if the layer is built correctly.
     * 
     * @return True if no problem is found, and False otherwise.
     */
    bool IfLayerIsBuiltCorrectly();
    /**
     * @brief Checks requirements for metal packages.
     * Essentially, requirement C12 from the problem.
     * 
     * @return True if required is satisfied, and False otherwise.
     */
    bool CheckMetalPackages();

private:
    /**
     * @brief Sorts children ids by coordinates.
     */
    void SortByCoordinates();

private:
    /// @brief The reference to the solution layer for verification.
    const OutputLayer& outputLayer;
    /// @brief The reference to the vector with solution rows.
    const std::vector<OutputRow>& outputRows;
    /// @brief The reference to the context with requirements and parameters.
    const Context& context;
    /// @brief The reference to the vector with child ids ordered by coordinates.
    std::vector<unsigned> orderedChildrenIds;
};


/**
 * @brief Checker responsible for verifying rows.
 */
class RowChecker{
public:
    /**
     * @brief Constructor for Row Checker.
     * 
     * @param row The solution row to verify.
     * @param itemsVector The vector with solution items.
     * @param context The context with requirements and parameters.
     */
    RowChecker(
        const OutputRow& row, 
        const std::vector<OutputItem>& itemsVector, 
        const Context& context);
    /**
     * @brief Checks for the maximal amount of items in the row allowed.
     * Essentially, requirement C21 from the statement.
     * 
     * @return True if the requirement is satisfied, and False otherwise.
     */
    bool CheckMaxItemsAllowed();
    /**
     * @brief Checks if the dimensions of all items in the row are equal.
     * Essentially, requirement C23 from the statement.
     * 
     * @return True if the requirement is satisfied, and False otherwise.
     */
    bool IfItemsDimensionsAreEqual();
    /**
     * @brief Checks if all items in the row have the same height.
     * Essentially, requirement C18 from the statement.
     * 
     * @return True if the requirement is satisfied, and False otherwise.
     */
    bool IfAllItemsHaveSameHeight();
    /**
     * @brief Checks if row is built correctly.
     * 
     * @return True if no problem is found, and False otherwise.
     */
    bool IfRowIsBuiltCorrectly();
    /**
     * @brief Checks if items are positioned correctly.
     * 
     * @return True if no problem is found, and False otherwise.
     */
    bool IfItemsPositionCorrectly();
    /**
     * @brief Checks if metal packages satisfy the requirement C12.
     * 
     * @return True if the requirement is satisfied, and False otherwise.
     */
    bool CheckMetalPackages();

private:
    /// @brief The reference to solution row for verification.
    const OutputRow& outputRow;
    /// @brief The reference to vector with solution items.
    const std::vector<OutputItem>& outputItems;
    /// @brief The reference to context describing parameters and requirements.
    const Context& context;
    /// @brief Vector with child ids ordered according to coordinates.
    std::vector<unsigned> orderedChildrenIds;
};

/**
 * @brief Comparator for item indices. 
 * Sorts according to (x, y) coordinate 
 * pair of the origin at corresponding item.
 */
class ItemOriginComparator {
public:
    /**
     * @brief Constructor for Item Origin Comparator.
     * 
     * @param itemsVector The vector with solution items.
     */
    ItemOriginComparator(const std::vector<OutputItem>& itemsVector) : items(itemsVector) {}
    /**
     * @brief Compares indices of the solution items.
     * 
     * @param idx The index of the first item.
     * @param jnd The index of the second item.
     * 
     * @return True if the first index is sorted earlier, and False otherwise.
     */
    bool operator() (unsigned idx, unsigned jnd) {
        if (items[idx].origin.x != items[jnd].origin.x) {
            return items[idx].origin.x < items[jnd].origin.x;
        }
        return items[idx].origin.y < items[jnd].origin.y;
    }

private:
    /// @brief The reference to a vector with solution items.
    const std::vector<OutputItem>& items;
};

/**
 * @brief Compares solution row indices by the origins 
 * of the corresponding solution rows.
 */
class RowOriginComparator {
public:
    /**
     * @brief Constructor for the Row Origin Comparator.
     * 
     * @param rowsVector The vector with solution rows.
     */
    RowOriginComparator(const std::vector<OutputRow>& rowsVector) : rows(rowsVector) {}
    /**
     * @brief Compares indices of the solution rows.
     * 
     * @param idx The index of the first row.
     * @param jnd The index of the second row.
     * 
     * @return True if the first index is sorted earlier, and False otherwise.
     */
    bool operator()(unsigned idx, unsigned jnd) {
        if (rows[idx].origin.x != rows[jnd].origin.x) {
            return rows[idx].origin.x < rows[jnd].origin.x;
        }
        return rows[idx].origin.y < rows[jnd].origin.y;
    }

private:
    /// @brief The reference to a vector with solution rows.
    const std::vector<OutputRow>& rows;
};

/**
 * @brief Compares solution layer indices by the 
 * weights of corresponding solution layers.
 */
class LayerWeightComparator{
public:
    /**
     * @brief Constructor for the Layer Weight Comparator.
     * 
     * @param layerVector The vector with solution layers.
     */
    LayerWeightComparator(const std::vector<OutputLayer>& layerVector) : layers(layerVector) {}
    /**
     * @brief Compares indices of the solution layers, 
     * according to (-weight, z-coordinate) pairs.
     * 
     * @param idx The index of the first layer.
     * @param jnd The index of the second layer.
     * 
     * @return True if the first index is sorted earlier, and False otherwise.
     */
    bool operator()(unsigned idx, unsigned jnd) const {
        if (layers[idx].GetWeight() != layers[jnd].GetWeight()) {
            return layers[idx].GetWeight() > layers[jnd].GetWeight();
        } 
        return layers[idx].origin.z < layers[jnd].origin.z;
    }

private:
    /// @brief The reference to a vector with solution layers.
    const std::vector<OutputLayer>& layers;
};

} // namespace checker
} // namespace esicup
