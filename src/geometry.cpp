/// Implements API defined in geometry.h.
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "geometry.h"

namespace esicup {
namespace geometry {

PosedRectangle GetEnvelope(const std::vector<PosedRectangle>& figures) {
    if (!figures.size()) {
        throw "Bad input in GetEnvelope!";
    }
    Point2D leftBottom = figures[0].origin;
    Point2D rightTop = figures[0].origin;
    for (auto& item : figures) {
        leftBottom.x = std::min(leftBottom.x, item.origin.x);
        leftBottom.y = std::min(leftBottom.y, item.origin.y);
        rightTop.x = std::max(rightTop.x, item.origin.x + item.rectangle.xLen);
        rightTop.y = std::max(rightTop.y, item.origin.y + item.rectangle.yLen);
    }
    return PosedRectangle(leftBottom, rightTop);
}

bool IfHaveIntersection(
        Point3D firstOrigin,
        Point3D firstExtremity,
        Point3D secondOrigin,
        Point3D secondExtremity) {
    auto minRightX = std::min(firstExtremity.x, secondExtremity.x),
        maxLeftX = std::max(firstOrigin.x, secondOrigin.x),
        minRightY = std::min(firstExtremity.y, secondExtremity.y),
        maxLeftY = std::max(firstOrigin.y, secondOrigin.y),
        minRightZ = std::min(firstExtremity.z, secondExtremity.z),
        maxLeftZ = std::min(firstOrigin.z, secondOrigin.z);
    return (minRightX < maxLeftX && minRightY < maxLeftY &&
            minRightZ < maxLeftZ);
}

} // namespace geometry
} // namespace esicup


