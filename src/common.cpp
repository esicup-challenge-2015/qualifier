/// Implements API and structures defined in common.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "common.h"
#include "checker.h"

#include <utility>
#include <string>
#include <algorithm>
#include <vector>
#include <cassert>


double GetMaxBinVolume(const InputData& dataInput) {
    double maximalVolume = 0.0;
    auto initialized = false;
    for (const auto& value : dataInput.bins) {
        const double THOUSAND = 1000.;
        double candidate = (value.length / THOUSAND) 
                * (value.width  / THOUSAND) * (value.height / THOUSAND);
        if (!initialized || candidate > maximalVolume) {
            maximalVolume = candidate;
        }
    }
    return maximalVolume;
}

void ProcessInstances(ParametersCLI inputParameters) {
    std::cout << std::fixed;
    std::cout.precision(8);
    std::cerr << std::fixed;
    std::cerr.precision(8);
    auto instanceNames = esicup::io::InitialRead(inputParameters.pathToDirectory);

    double setVolumeSum = 0.0, setObjectiveSum = 0.0, setLowerBoundSum = 0.0;
    std::vector<std::pair<std::string, std::vector<double>>> summary;
    for (auto dirname : instanceNames) {
        double instanceVolumeSum = 0.0;
        
        Reader reader;
        InputData data = reader.ReadData(inputParameters.pathToDirectory + "/" +  dirname);
        for (const auto& item : data.items) {
            const double THOUSAND = 1000.;
            instanceVolumeSum +=
                (item.width / THOUSAND) * (item.height / THOUSAND) * (item.length / THOUSAND);
        }

        auto startCPUtime = clock();
        esicup::algo::HeuristicPacker packer;
        OutputData resultPacking = packer.Process(data);
        auto finishCPUtime = clock();

        std::cerr << "CPU Time = "
            << static_cast<double>(finishCPUtime - startCPUtime) / CLOCKS_PER_SEC
            << " seconds." << std::endl;

        esicup::common::OutputCompleter filler(data);
        filler.Fill(resultPacking);
        esicup::checker::ObjectiveCalculator scorer(resultPacking);
        scorer.Compute();
        auto resultObjective = scorer.GetObjective();
        auto instanceLowerBound = std::max(instanceVolumeSum - GetMaxBinVolume(data),
                instanceVolumeSum * (1. - data.context.tolerance_item_product_demands));

        std::cerr << dirname  << " First Objective = " << resultObjective.objectives[0] << std::endl;
        std::cerr << "Item volume sum in directory = " << instanceVolumeSum << std::endl;
        std::cerr << "LowerBound  = " << instanceLowerBound << std::endl;

        summary.push_back({dirname, resultObjective.objectives});
        setObjectiveSum += resultObjective.objectives[0];
        setVolumeSum += instanceVolumeSum;
        setLowerBoundSum += instanceLowerBound;

        Writer writer;
        writer.WriteData(resultPacking, inputParameters.pathToDirectory + "/" +  dirname);

        auto checker = esicup::checker::GlobalChecker(resultPacking, data);
        assert(checker.CheckFeasibility());
    }

    std::cout << "Instance Summary. " << std::endl;
    for (const auto& entry: summary) {
        std::cout << entry.first << " ";
        for (const auto& item: entry.second) {
            std::cout << item << " ";
        }
        std::cout << std::endl;
    }

    std::cout << "Total Objective 1 = " << setObjectiveSum << std::endl;
    std::cout << "Item Volume Sum = " << setVolumeSum << std::endl;
    std::cout << "Lower Bound Sum = " << setLowerBoundSum << std::endl;
}

ParametersCLI ParseInputFromCLI(int argc, char** argv) {
    ParametersCLI parsedParameters;
    parsedParameters.initOk = true;

    std::vector<std::pair<std::string, std::string>> parameterPairs;
    const int ARGUMENT_COUNT = 3;
    if (argc != ARGUMENT_COUNT) {
        parsedParameters.initOk = false;
    } else {
        for (int idx = 1; idx < ARGUMENT_COUNT; idx += 2) {
            parameterPairs.push_back(
                std::make_pair<std::string, std::string>(argv[idx], argv[idx + 1]));
        }
        std::sort(parameterPairs.begin(), parameterPairs.end());
        if (parameterPairs[0].first != "-p") {
            parsedParameters.initOk = false;
        } else {
            parsedParameters.pathToDirectory = parameterPairs[0].second;
        }
    }

    return parsedParameters;
}

