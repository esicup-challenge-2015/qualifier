/// Implements API and structures defined in input.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "input.h"

namespace esicup {
namespace common {

double ConvertToDouble(const std::string& strToConvert) {
    std::istringstream stream(strToConvert);
    double value;
    if (!(stream >> value)) {
        throw std::logic_error("convertToDouble(\"" + strToConvert + "\")");
    }
    return value;
}

std::string MaterialToString(Material material) {
  switch (material) {
    case Material::METAL: {
      return "METAL";
    } case Material::CARDBOARD: {
      return "CARDBOARD";
    } case Material::PLASTIC: {
      return "PLASTIC";
    } case Material::WOOD: {
      return "WOOD";
    } case Material::UNDEFINED: {
      return "UNDEFINED";
    }
  }
  throw std::logic_error("Unexpected material!");
}

Material StringToMaterial(const std::string& str) {
  if (str == "METAL") {
    return Material::METAL;
  } else if (str == "CARDBOARD") {
    return Material::CARDBOARD;
  } else if (str == "WOOD") {
    return Material::WOOD;
  } else  if (str == "PLASTIC") {
    return Material::PLASTIC;
  } else {
    throw std::logic_error("Unexpected material!");
  }
}

void Context::FillContext(const std::vector<std::string>& linesVector) {
    for (auto& line : linesVector) {
        std::istringstream stream(line);
        std::string key, value;
        getline(stream, key, '=');
        getline(stream, value, '=');
        if (key == "tolerance_item_product_demands") {
            tolerance_item_product_demands = ConvertToDouble(value);
        } else if (key == "maximum_density_allowed_kg") {
            maximum_density_allowed_kg = ConvertToDouble(value);
        } else if (key == "max_number_of_rows_in_a_layer") {
            max_number_of_rows_in_a_layer = std::stoi(value);
        } else if (key == "max_number_of_items_in_a_row") {
            max_number_of_items_in_a_row = std::stoi(value);
        } else if (key == "layer_size_deviation_tolerance") {
            layer_size_deviation_tolerance = ConvertToDouble(value);
        } else if (key == "row_size_deviation_tolerance") {
            row_size_deviation_tolerance = ConvertToDouble(value);
        } else if (key == "item_size_deviation_tolerance") {
            item_size_deviation_tolerance = ConvertToDouble(value);
        } else if (key == "maximum_weigth_above_item_kg") {
            maximum_weigth_above_item_kg = ConvertToDouble(value);
        }
    }
}

std::string Context::ToString() const {
    std::stringstream stream;
    stream << "tolerance_item_product_demands="
        << tolerance_item_product_demands
        << "\nmaximum_density_allowed_kg="
        << maximum_density_allowed_kg
        << "\nmax_number_of_rows_in_a_layer="
        << max_number_of_rows_in_a_layer
        << "\nmax_number_of_items_in_a_row="
        << max_number_of_items_in_a_row
        << "\nlayer_size_deviation_tolerance="
        << layer_size_deviation_tolerance
        << "\nrow_size_deviation_tolerance="
        << row_size_deviation_tolerance
        << "\nitem_size_deviation_tolerance="
        << item_size_deviation_tolerance
        << "\nmaximum_weigth_above_item_kg="
        << maximum_weigth_above_item_kg;
    return stream.str();
}

void InputData::BuildProductCount() {
    productCount.clear();
    for (const auto& entry : items) {
        ++productCount[entry.productId];
    }
    context.productCount = productCount;
}

} // namespace common
} // namespace esicup
