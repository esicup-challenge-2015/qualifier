/// Implements methods and API defined in writer.h.
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "writer.h"

namespace esicup {
namespace io {

void Writer::WriteData(const OutputData& data, const std::string& directory) {
    const std::string nameBinsFile = directory + "/bins.csv";
    const std::string nameStacksFile = directory + "/stacks.csv";
    const std::string nameLayersFile = directory + "/layers.csv";
    const std::string nameRowsFile = directory + "/rows.csv";
    const std::string nameItemsFile = directory + "/items.csv";

    WriteBins(data.bins, nameBinsFile);
    WriteStacks(data.stacks, nameStacksFile);
    WriteLayers(data.layers, nameLayersFile);
    WriteRows(data.rows, nameRowsFile);
    WriteItems(data.items, nameItemsFile);
}

void Writer::WriteBins(const std::vector<OutputBin>& bins, const std::string& fileName) {
    const std::string format = "id bin;bin type;";

    std::ofstream output(fileName);
    output << format << std::endl;

    for (const auto& package : bins){
        std::ostringstream stream;
        stream << package.id << ';' << package.type;
        output << stream.str() << std::endl;
    }

    output.close();
}

void Writer::WriteStacks(const std::vector<OutputStack>& stacks, const std::string& fileName) {
    const std::string format = "id stack;id bin;xO;yO;zO;xE;yE;zE;";

    std::ofstream output(fileName);
    output << format << std::endl;

    for (const auto& package : stacks) {
        std::ostringstream stream;
        stream << package.id << ';'
            << package.idParent << ';'
            << package.origin.x << ';'
            << package.origin.y << ';'
            << package.origin.z << ';'
            << package.extremity.x << ';'
            << package.extremity.y << ';'
            << package.extremity.z;
        output << stream.str() << std::endl;
    }

    output.close();
}

void Writer::WriteLayers(const std::vector<OutputLayer>& layers, const std::string& fileName) {
    const std::string format = "id layer;id stack;xO;yO;zO;xE;yE;zE;flag top;";

    std::ofstream output(fileName);
    output << format << std::endl;

    for (const auto& package : layers){
        std::ostringstream stream;
        stream << package.id << ';'
            << package.idParent << ';'
            << package.origin.x << ';'
            << package.origin.y << ';'
            << package.origin.z << ';'
            << package.extremity.x << ';'
            << package.extremity.y << ';'
            << package.extremity.z << ';'
            << package.GetIsTop();
        output << stream.str() << std::endl;
    }

    output.close();
}

void Writer::WriteRows(const std::vector<OutputRow>& rows, const std::string& fileName) {
    const std::string format = "id row;id layer;xO;yO;zO;xE;yE;zE;";
    std::ofstream output(fileName);
    output << format << std::endl;

    for (const auto& package : rows){
        std::ostringstream stream;
        stream << package.id << ';'
            << package.idParent << ';'
            << package.origin.x << ';'
            << package.origin.y << ';'
            << package.origin.z << ';'
            << package.extremity.x << ';'
            << package.extremity.y << ';'
            << package.extremity.z;
        output << stream.str() << std::endl;
    }
    output.close();
}

void Writer::WriteItems(const std::vector<OutputItem>& items, const std::string& fileName) {
    const std::string format = "id item;id row;xO;yO;zO;xE;yE;zE;";

    std::ofstream output(fileName);
    output << format << std::endl;

    for (const auto& package : items){
        std::ostringstream stream;
        stream << package.id << ';'
            << package.idParent << ';'
            << package.origin.x << ';'
            << package.origin.y << ';'
            << package.origin.z << ';'
            << package.extremity.x << ';'
            << package.extremity.y << ';'
            << package.extremity.z;
        output << stream.str() << std::endl;
    }

    output.close();
}

} // namespace io
} // namespace esicup


