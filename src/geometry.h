/**
 * @file geometry.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines and partiall implements structures and API related to 
 * object geometry.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include <algorithm>
#include <utility>
#include <vector>

namespace esicup {
namespace geometry {

/**
 * @brief Represents point in 2-dimensional space.
 */
struct Point2D {
public:
    /**
     * @brief Constructor for 2-dimensional point.
     * 
     * @param valueX The value of x-coordinate.
     * @param valueY The value of y-coordinate.
     */
    Point2D(unsigned valueX = 0, unsigned valueY = 0) : x(valueX), y(valueY) {}

    /**
     * @brief Translates this point in the requested direction.
     * 
     * @param direction The vector of translation.
     */
    void Translate(Point2D direction) {
        x += direction.x;
        y += direction.y;
    }

    /**
     * @brief Creates a point obtained by translating this point
     * in the requested direction.
     * 
     * @param direction The direction of translation.
     * 
     * @return The new point, obtained by translating this point
     * in the requested direction.
     */
    Point2D GetTranslated(Point2D direction) const {
        direction.Translate(*this);
        return direction;
    }
    /**
     * @brief Virtual destructor for correct polymorphism.
     */
    virtual ~Point2D() {}

public:
    /// @brief The x-coordinate of the point.
    unsigned x;
    /// @brief The y-coordinate of the point.
    unsigned y;
};

/**
 * @brief Represents point in 3-dimensional space.
 * Briefly, the 3-dimensional point is 2-dimensional point 
 * with 1-dimensional coordinate corresponding to Z axis.
 */
struct Point3D : public Point2D {
public:
    /**
     * @brief Constructor for 3-dimensional point.
     * 
     * @param valueX The X-coordinate.
     * @param valueY The Y-coordinate.
     * @param valueZ The Z-coordinate.
     */
    Point3D(unsigned valueX = 0, unsigned valueY = 0, unsigned valueZ = 0)
        : Point2D(valueX, valueY)
        , z(valueZ) {}
    /**
     * @brief Translates this point in the requested direction.
     * 
     * @param direction The vector of translation.
     */
    void Translate(Point3D direction) {
        x += direction.x;
        y += direction.y;
        z += direction.z;
    }
    /**
     * @brief Creates a point obtained by translating this point
     * in the requested direction.
     * 
     * @param direction The direction of translation.
     * 
     * @return The new point, obtained by translating this point
     * in the requested direction.
     */
    Point3D GetTranslated(Point3D direction) const {
        direction.Translate(*this);
        return direction;
    }

public:
    /// @brief The z-coordinate of the point.
    unsigned z;
};

/**
 * @brief Rectangle is a structure for storing 2-dimensional rectangles
 * and providing related API. Essentially, stores lengths along x and y
 * axes.
 */
struct Rectangle {
public:
    /**
     * @brief Constructor for 2-dimensional rectangle.
     * 
     * @param valueXLen Length along x coordinate axis.
     * @param valueYLen Length along y coordinate axis.
     */
    Rectangle(unsigned valueXLen = 0, unsigned valueYLen = 0)
        : xLen(valueXLen)
        , yLen(valueYLen) {}

    /**
     * @brief Rotates rectangle to 90 degrees. 
     * Essentially, swaps axes.
     */
    void Rotate() {
        std::swap(xLen, yLen);
    }

    /**
     * @brief Creates rectangle, obtained by rotating
     * this rectangle 90 degrees.
     * 
     * @return A rectangle obtained by rotating this 
     * rectangle over 90 degrees.
     */
    Rectangle GetRotated() const {
        return Rectangle(yLen, xLen);
    }

    /**
     * @brief Computes rectangle area.
     * 
     * @return The area of the rectangle.
     */
    unsigned long long GetArea() const {
        return static_cast<unsigned long long>(xLen) * static_cast<unsigned long long>(yLen);
    }

    /**
     * @brief Checks whether the rectangle is empty.
     * Rectangle is empty when both lengths over x-axis 
     * and over y-axis are zero.
     * 
     * @return True if the rectangle is empty, and False otherwise.
     */
    bool IsEmpty() const {
        return xLen == 0 || yLen == 0;
    }

public:
    /// @brief Length over x-coordinate.
    unsigned xLen;
    /// @brief Length over y-coordinate.
    unsigned yLen;
};

/**
 * @brief PosedRectangle is a rectangle in 2-dimensional space 
 * together with origin point (bottom left point).
 */
struct PosedRectangle {
public:
    /**
     * @brief Constructor for a Posed Rectangle 
     * from 2-dimensional point and rectangle.
     * 
     * @param originPoint The point of origin (bottom left point).
     * @param initRectangle The rectangle object, storing length and width.
     */
    PosedRectangle(
            Point2D originPoint, 
            Rectangle initRectangle)
            : origin(originPoint)
            , rectangle(initRectangle) {}

    /**
     * @brief Constructor for a Posed Rectangle 
     * from two points.
     * 
     * @param sourceVertex Coordinate of rectangle vertex.
     * @param oppositeVertex Coordinate of the opposite vertex.
     */
    PosedRectangle(Point2D sourceVertex, Point2D oppositeVertex) {
        Initialize(sourceVertex.x, sourceVertex.y, 
            oppositeVertex.x, oppositeVertex.y);
    }
    /**
     * @brief Default constructor for Posed Rectangle.
     * Initializes empty rectangle located at axis origin.
     */
    PosedRectangle() {
        Initialize(0, 0, 0, 0);
    }
    /**
     * @brief Computes intersection of this and other posed rectangles.
     * 
     * @param other The other posed rectangle to intersect with.
     * 
     * @return Posed rectangle representing intersection. When no intersection
     *          returns empty posed rectangle at axis origin. 
     */
    PosedRectangle GetIntersection(const PosedRectangle& other) const {
        unsigned sourceLeftX = origin.x;
        unsigned sourceBottomY = origin.y;
        unsigned sourceRightX = sourceLeftX + rectangle.xLen;
        unsigned sourceTopY = sourceBottomY + rectangle.yLen;

        unsigned otherLeftX = other.origin.x;
        unsigned otherBottomY = other.origin.y;
        unsigned otherRightX = otherLeftX + other.rectangle.xLen;
        unsigned otherTopY = otherBottomY + other.rectangle.yLen;

        unsigned maxLeftX = std::max(sourceLeftX, otherLeftX);
        unsigned minRightX = std::min(sourceRightX, otherRightX);
        unsigned maxBottomY = std::max(sourceBottomY, otherBottomY);
        unsigned minTopY = std::min(sourceTopY, otherTopY);

        if (maxLeftX >= minRightX || maxBottomY >= minTopY) {
            // there is no intersection
            // returning fictional rectangle
            return PosedRectangle(Point2D(0, 0), Rectangle(0, 0));
        }
        return PosedRectangle(Point2D(maxLeftX, maxBottomY), Rectangle(minRightX, minTopY));
    }

    /**
     * @brief Checks if this rectangle has non-empty intersection with another rectangle.
     * 
     * @param another The other rectangle to check for intersection.
     * 
     * @return True if there is non-empty intersection, and False otherwise. 
     */
    bool IfHaveIntersection(const PosedRectangle& another) const {
        return !GetIntersection(another).rectangle.IsEmpty();
    }

private:
    /**
     * @brief Initializes posed rectangle by coordinates of opposite vertices.
     * 
     * @param xVertex X-coordinate of the rectangle vertex.
     * @param yVertex Y-coordinate of the rectangle vertex.
     * @param xOpposite X-coordinate of the opposite vertex.
     * @param yOpposite Y-coordinate of the opposite vertex.
     */
    void Initialize(unsigned xVertex, unsigned yVertex, 
                    unsigned xOpposite, unsigned yOpposite) {
        if (xVertex > xOpposite) {
            std::swap(xVertex, xOpposite);
        }
        if (yVertex > yOpposite) {
            std::swap(yVertex, yOpposite);
        }
        origin = Point2D(xVertex, yVertex);
        rectangle = Rectangle(xOpposite - xVertex, yOpposite - yVertex);
    }

public:
    /// @brief Coordinate of the origin point (left bottom vertex).
    Point2D origin;
    /// @brief Rectangle storing lengths over x and y axes.
    Rectangle rectangle;
};

/**
 * @brief Builds rectangle that envelopes the set of posed rectangles.
 * 
 * @param figures The vector with figures to build envelope around.
 * @return Posed rectangle enveloping figures.
 */
PosedRectangle GetEnvelope(const std::vector<PosedRectangle>& figures);

/**
 * @brief Checks if two 3-dimensional boxes have interesections.
 * 
 * @param firstOrigin The origin of first 3-dimensional box.
 * @param firstExtremity The extremity of first 3-dimensional box.
 * @param secondOrigin The origin of second 3-dimensional box.
 * @param secondExtremity The extremity of second 3-dimensional box.
 * 
 * @return True if the intersection is non-empty, False otherwise.
 */
bool IfHaveIntersection(
        Point3D firstOrigin, Point3D firstExtremity, Point3D secondOrigin, Point3D secondExtremity);

} // namespace geometry
} // namespace esicup
