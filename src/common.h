/**
 * @file common.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Describes CLI parsing and commonly used methods/API.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */

#include "output.h"
#include "checker.h"
#include "objective.h"
#include "geometry.h"
#include "writer.h"
#include "reader.h"
#include "input.h"
#include "algo/heuristic.h"

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <utility>

using namespace esicup::io;

/**
 * @brief Method to get maximal bin volume in the data.
 *
 * @param data Structure storing the data.
 *
 * @returns The maximal bin volume.
 */
double GetMaxBinVolume(const InputData& data);

/**
 * @brief Structure to store CLI information.
 */
struct ParametersCLI {
public:
    /// @brief The path to the root directory with multiple instances.
    std::string pathToDirectory;
    /// @brief Whether the reading CLI parameters went OK.
    bool initOk;
};

/**
 * @brief Processes instances described by input parameters.
 * 
 * @param inputParameters Parameters parsed from CLI.
 */
void ProcessInstances(ParametersCLI inputParameters);

/**
 * @brief Parses command line arguments.
 * 
 * @param argc The number of arguments.
 * @param argv The array with arguments.
 * 
 * @return Structure summarizing the arguments parsed.
 */
ParametersCLI ParseInputFromCLI(int argc, char** argv);


