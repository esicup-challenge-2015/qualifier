/**
 * @file writer.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines Writer - entity to export packing to files.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 * 
 */
#pragma once

#include <fstream>
#include <string.h>
#include "checker.h"

#include "input.h"

namespace esicup {
namespace io {

using esicup::common::OutputBin;
using esicup::common::OutputStack;
using esicup::common::OutputLayer;
using esicup::common::OutputRow;
using esicup::common::OutputItem;
using esicup::common::OutputData;

/**
 * @brief Writer is an entity to export packing to corresponding files.
 */
class Writer {
public:
    /**
     * @brief Entry point for writing output data. Specifies file names 
     * and calls corresponding private methods to write data by parts.
     * 
     * @param dataOutput The data to export to files.
     * @param directory The directory for exporting.
     */
    void WriteData(const OutputData& dataOutput, const std::string& directory);
    
private:
    /**
     * @brief Writes bins to file.
     * 
     * @param binsToWrite The vector with filled bins to export.
     * @param fileName The name of the file for exporting bins.
     */
    void WriteBins(const std::vector<OutputBin>& binsToWrite, const std::string& fileName);
    /**
     * @brief Writes stacks to file.
     * 
     * @param stacksToWrite The vector with stacks to export.
     * @param fileName The name of the file for exporting stacks.
     */
    void WriteStacks(const std::vector<OutputStack>& stacksToWrite, const std::string& fileName);
    /**
     * @brief Writes layers to file.
     * 
     * @param layersToWrite The vector with layers to export.
     * @param fileName The name of the file for exporting layers.
     */
    void WriteLayers(const std::vector<OutputLayer>& layersToWrite, const std::string& fileName);
    /**
     * @brief Writes rows to file.
     * 
     * @param rowsToWrite The vector with rows to export.
     * @param fileName The name of the file for exporting rows.
     */
    void WriteRows(const std::vector<OutputRow>& rowsToWrite, const std::string& fileName);
    /**
     * @brief Writes items to file.
     * 
     * @param itemsToWrite The vector with items to export.
     * @param fileName The name of the file for exporting items.
     */
    void WriteItems(const std::vector<OutputItem>& itemsToWrite, const std::string& fileName);
};

} // namespace io
} // namespace esicup
