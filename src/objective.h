/**
 * @file objective.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines and implements API related to computing problem-specific objectives.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "checker.h"

namespace esicup {
namespace checker {

/**
 * @brief Structure to represent problem-specific objective.
 */
struct Objective{
public:
    /**
     * @brief Objective constructor.
     */
    Objective();
    /**
     * @brief Compares objective to another objective.
     * 
     * @param other The other objective instance.
     * 
     * @return True if this objective is more effective, and False otherwise.
     */
    bool operator<(const Objective& other) const;

public:
    /// @brief The value of big floating number for initializations.
    static const double INFINITY;
    /// @brief The number of different objectives.
    static const unsigned N_OBJECTIVES;
    /// @brief Structure to store the values of the objectives.
    std::vector<double> objectives;
};

/**
 * @brief Entity to compute and retrieve objective value.
 */
class ObjectiveCalculator{
public:
    /**
     * @brief Constructor for Objective Calculator.
     * 
     * @param dataOutput The Output Data to store 
     *          and further compute objective from. 
     */
    ObjectiveCalculator(const OutputData& dataOutput);
    /**
     * @brief Computes the objectives and stores 
     * them in the objective class member.
     */
    void Compute();
    /**
     * @brief Retrieves the objective class member.
     * 
     * @return The currently stored objective value.
     */
    Objective GetObjective() const;

private:
    /// @brief The output data to compute the objective from.
    const OutputData& outputData;
    /// @brief Class member to store objective value.
    Objective objective;
    /// @brief Method to compute the first objective.
    void ComputeFirstObjective();
    /// @brief Method to compute the second objective.
    void ComputeSecondObjective();
};

} // namespace checker
} // namespace esicup
