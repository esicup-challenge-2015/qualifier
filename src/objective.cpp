/// Defines and implements API mentioned in objective.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "objective.h"

#include <limits>

namespace esicup{
namespace checker{

const double Objective::INFINITY = std::numeric_limits<double>::max();
const unsigned Objective::N_OBJECTIVES = 2;

Objective::Objective() {
    objectives.resize(N_OBJECTIVES);
    for (unsigned idx = 0; idx < N_OBJECTIVES; ++idx) {
        objectives[idx] = INFINITY;
    }
}

bool Objective::operator<(const Objective& another) const {
    return objectives < another.objectives;
}

ObjectiveCalculator::ObjectiveCalculator(const OutputData& outputInit) : outputData(outputInit) {}

void ObjectiveCalculator::Compute(){
    ComputeFirstObjective();
    ComputeSecondObjective();
}

Objective ObjectiveCalculator::GetObjective() const {
    return objective;
}

void ObjectiveCalculator::ComputeFirstObjective() {
    double totalVolume = 0;
    for (unsigned idx = 1; idx < outputData.bins.size(); ++idx) {
        double xDelta = outputData.bins[idx].extremity.x - outputData.bins[idx].origin.x;
        double yDelta = outputData.bins[idx].extremity.y - outputData.bins[idx].origin.y;
        double zDelta = outputData.bins[idx].extremity.z - outputData.bins[idx].origin.z;
        totalVolume += xDelta * yDelta * zDelta;
    }
    const double THOUSAND = 1000.;
    objective.objectives[0] = totalVolume / THOUSAND / THOUSAND / THOUSAND;
}

void ObjectiveCalculator::ComputeSecondObjective() {
    double volumeBinZero = 0;
    const OutputBin& binZero = outputData.bins[0];
    for (unsigned idx = 0; idx < binZero.GetCountChildren(); ++idx) {
        volumeBinZero += GetVolume(outputData.stacks[binZero.GetChildIdByIndex(idx)].origin,
                                outputData.stacks[binZero.GetChildIdByIndex(idx)].extremity);
    }
    objective.objectives[1] = volumeBinZero;
}

}
}
