/// Implements structures and API defined in checker.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "checker.h"

#include <algorithm>

namespace esicup{
namespace checker{

using geometry::PosedRectangle;
using geometry::Point2D;
using geometry::Point3D;
using common::InputData;

// in m^3
double GetVolume(Point3D origin, Point3D extremity) {
    const double THOUSAND = 1000.0;
    double width = (static_cast<double>(extremity.x) - static_cast<double>(origin.x)) / THOUSAND;
    double length = (static_cast<double>(extremity.y) - static_cast<double>(origin.y)) / THOUSAND;
    double height = (static_cast<double>(extremity.z) - static_cast<double>(origin.z)) / THOUSAND;

    return width * length * height;
}

GlobalChecker::GlobalChecker(
        const OutputData& outputInit, 
        const InputData& inputInit)
        : outputData(outputInit)
        , inputData(inputInit) {
}

std::map<std::string, unsigned> GlobalChecker::GetCountProductInBinZero() {
    std::map<std::string, unsigned> countProductBinZero;
    const OutputBin& binZero = outputData.bins[0];

    for (unsigned idxStack = 0; idxStack < binZero.GetCountChildren(); ++idxStack) {

        const OutputStack& outputStack = outputData.stacks[binZero.GetChildIdByIndex(idxStack)];
        for (unsigned idxLayer = 0; idxLayer < outputStack.GetCountChildren(); ++idxLayer) {

            const OutputLayer& outputLayer = outputData.layers[outputStack.GetChildIdByIndex(idxLayer)];
            for (unsigned idxRow = 0; idxRow < outputLayer.GetCountChildren(); ++idxRow) {

                const OutputRow& outputRow = outputData.rows[outputLayer.GetChildIdByIndex(idxRow)];
                for (unsigned idxItem = 0; idxItem < outputRow.GetCountChildren(); ++idxItem) {
                    ++countProductBinZero[outputData.items[outputRow.GetChildIdByIndex(idxItem)].GetProductId()];
                }
            }
        }
    }

    return countProductBinZero;
}

bool GlobalChecker::CheckCountProductInBinZero() {
    std::map<std::string, unsigned> productInBinZero = GetCountProductInBinZero();

    for (const auto& entry : productInBinZero) {
        auto productName = entry.first;
        auto productCount = entry.second;
        auto fullProductCount = inputData.productCount.at(productName);

        if (productCount > static_cast<unsigned>(
                inputData.context.tolerance_item_product_demands * fullProductCount)) {
            return false;
        }
    }

    return true;
}

bool GlobalChecker::IfNullBinHasSmallestVolume() {
    std::vector<double> binUsedVolumes;
    for (const auto& outputBin: outputData.bins) {
        double outputBinVolume = 0.;
        for (unsigned childIdx = 0; childIdx < outputBin.GetCountChildren(); ++childIdx) {
            unsigned stackIdx = outputBin.GetChildIdByIndex(childIdx);
            const auto& outputStack = outputData.stacks[stackIdx];
            outputBinVolume += GetVolume(outputStack.origin, outputStack.extremity);
        }
        binUsedVolumes.push_back(outputBinVolume);
    }

    for (unsigned idx = 1; idx < outputData.bins.size(); ++idx) {
        if (binUsedVolumes[0] > binUsedVolumes[idx]) {
            return false;
        }
    }
    return true;
}

//without bin 0
bool GlobalChecker::IfEveryBinIsNotEmpty(Logger& logger) {
    for (unsigned idx = 1; idx < outputData.bins.size(); ++idx) {
        if (outputData.bins[idx].GetCountChildren() == 0) {
            logger.AddToLog(outputData.bins[idx].id, "bin is empty");
            return false;
        }
    }
    return true;
}

bool GlobalChecker::IfEveryStackIsNotEmpty(Logger& logger) {
    for (unsigned idx = 0; idx < outputData.stacks.size(); ++idx) {
        if (outputData.stacks[idx].GetCountChildren() == 0) {
            logger.AddToLog(outputData.stacks[idx].id, "stack is empty");
            return false;
        }
    }
    return true;
}

bool GlobalChecker::IfEveryLayerIsNotEmpty(Logger& logger) {
    for (unsigned idx = 0; idx < outputData.layers.size(); ++idx) {
        if (outputData.layers[idx].GetCountChildren() == 0) {
            logger.AddToLog(outputData.layers[idx].id, "layer is empty");
            return false;
        }
    }
    return true;
}

bool GlobalChecker::IfEveryRowIsNotEmpty(Logger& logger) {
    for (unsigned idx = 0; idx < outputData.rows.size(); ++idx) {
        if (outputData.rows[idx].GetCountChildren() == 0) {
            logger.AddToLog(outputData.rows[idx].id, "row is empty");
            return false;
        }
    }
    return true;
}

template<typename Type>
bool GlobalChecker::IfEveryIdAppliedCorrectly(const std::vector<Type>& packages) {
    for (unsigned idx = 0; idx < packages.size(); ++idx) {
        if (packages[idx].id != idx) {
            return false;
        }
    }
    return true;
}

bool GlobalChecker::CheckBins(Logger& logger) {
    bool allRight = true;

    for (unsigned idx = 0; idx < outputData.bins.size(); ++idx) {
        BinChecker binChecker(outputData.bins[idx], outputData.stacks);
        if (!binChecker.CheckWeight()) {
            logger.AddToLog(outputData.bins[idx].id, "sum of the weights of the items \
                packed in the bin is larger then maximum weight allowed");
            allRight = false;
        }
        if (!binChecker.IfStacksLayEntirelyInside()) {
            logger.AddToLog(outputData.bins[idx].id, 
                "stacks have larger height then height of the bin");
            allRight = false;
        }
        if (!binChecker.IfStacksNonOverlap()) {
            logger.AddToLog(outputData.bins[idx].id, 
                "stacks are overlapped");
            allRight = false;
        }
    }
    return allRight;
}

bool GlobalChecker::CheckStacks(Logger& logger) {
    bool allRight = true;
    for (unsigned idx = 0; idx < outputData.stacks.size(); ++idx) {
        //check parent id
        if (outputData.stacks[idx].idParent >= outputData.bins.size()) {
            logger.AddToLog(outputData.stacks[idx].id, "parent id incorrectly");
            allRight = false;
        }

        StackChecker stackChecker(outputData.stacks[idx],
            outputData.layers, outputData.rows, outputData.items, outputData.context);

        if (!stackChecker.IfLayersAreSortedByWeights()) {
            logger.AddToLog(outputData.stacks[idx].id, "layers unsorted");
            allRight = false;
        }
        if (!stackChecker.CheckMetalPackages()) {
            logger.AddToLog(outputData.stacks[idx].id, "stack has metal and nonmetal layers");
            allRight = false;
        }
        if (!stackChecker.IfStackIsBuiltCorrectly()) {
            logger.AddToLog(outputData.stacks[idx].id, "stack built incorrectly");
            allRight = false;
        }
        if (!stackChecker.CheckDensity()) {
            logger.AddToLog(outputData.stacks[idx].id, "exceeded the maximum density allowed");
            allRight = false;
        }
        if (!stackChecker.IfLayersDimensionsAreEqual()) {
            logger.AddToLog(outputData.stacks[idx].id, "layers have different dimensions in the stack");
            allRight = false;
        }
        if (!stackChecker.CheckWeightOnFundingLayerItems()) {
            logger.AddToLog(outputData.stacks[idx].id, "exceeded the maximum weight on base layer");
            allRight = false;
        }
    }
    return allRight;
}

bool GlobalChecker::CheckLayers(Logger& logger) {
    bool allRight = true;

    for (unsigned idx = 0; idx < outputData.layers.size(); ++idx) {
        //check parent id
        if (outputData.layers[idx].idParent >= outputData.stacks.size()) {
            logger.AddToLog(outputData.layers[idx].id, "parent id incorrectly");
            allRight = false;
        }

        LayerChecker checkLayer(outputData.layers[idx], outputData.rows, outputData.context);

        if (!checkLayer.IfAllRowsHaveSameHeight()){
            logger.AddToLog(outputData.layers[idx].id, 
                "rows have different heights");
            allRight = false;
        }
        if (!checkLayer.CheckMaxRowsAllowed()){
            logger.AddToLog(outputData.layers[idx].id, 
                "exceeded the maximum number of rows in a layer");
            allRight = false;
        }
        if (!checkLayer.CheckMetalPackages()){
            logger.AddToLog(outputData.layers[idx].id, 
                "layer has metal and nonmetal rows");
            allRight = false;
        }
        if (!checkLayer.IfLayerIsBuiltCorrectly()){
            logger.AddToLog(outputData.layers[idx].id, 
                "layer is built incorrectly");
            allRight = false;
        }
        if (!checkLayer.IfRowsDimensionsAreEqual()){
            logger.AddToLog(outputData.layers[idx].id, 
                "rows have different dimensions in the layer");
            allRight = false;
        }
    }
    return allRight;
}

bool GlobalChecker::CheckRows(Logger& logger) {

    bool allRight = true;

    for (unsigned idx = 0; idx < outputData.rows.size(); ++idx){
        //check parent id
        if (outputData.rows[idx].idParent >= outputData.layers.size()) {
            logger.AddToLog(outputData.rows[idx].id, 
                "parent id is assigned incorrectly");
            allRight = false;
        }

        RowChecker rowChecker(outputData.rows[idx], outputData.items, outputData.context);

        if (!rowChecker.IfAllItemsHaveSameHeight()) {
            logger.AddToLog(outputData.rows[idx].id, 
                "items have different heights");
            allRight = false;
        }
        if (!rowChecker.CheckMaxItemsAllowed()) {
            logger.AddToLog(outputData.rows[idx].id, 
                "exceeded the maximum number of items in a row");
            allRight = false;
        }
        if (!rowChecker.CheckMetalPackages()) {
            logger.AddToLog(outputData.rows[idx].id, 
                "row has metal and nonmetal items");
            allRight = false;
        }
        if (!rowChecker.IfRowIsBuiltCorrectly()) {
            logger.AddToLog(outputData.rows[idx].id, 
                "row is built incorrectly");
            allRight = false;
        }
        if (!rowChecker.IfItemsDimensionsAreEqual()) {
            logger.AddToLog(outputData.rows[idx].id, 
                "items have different dimensions in the row");
            allRight = false;
        }
        if (!rowChecker.IfItemsPositionCorrectly()) {
            logger.AddToLog(outputData.rows[idx].id, 
                "some items are located in the wrong position");
            allRight = false;
        }
    }
    return allRight;
}

bool GlobalChecker::IfEachItemIsPacked() {
    return outputData.items.size() == inputData.items.size();
}

bool GlobalChecker::CheckFeasibility() {
    if (!IfEachItemIsPacked()) {
        std::cerr << "not every item is packed" << std::endl;
        return false;
    }

    if (!IfEveryIdAppliedCorrectly(outputData.bins) || 
            !IfEveryIdAppliedCorrectly(outputData.stacks) ||
            !IfEveryIdAppliedCorrectly(outputData.layers) ||
            !IfEveryIdAppliedCorrectly(outputData.rows) ||
            !IfEveryIdAppliedCorrectly(outputData.items)) {
        std::cerr << "ids assigned incorrectly" << std::endl;
        return false;
    }

    Logger loggerBin, loggerStack, loggerLayer, loggerRow;

    if (!IfEveryBinIsNotEmpty(loggerBin) || !CheckBins(loggerBin)) {
        loggerBin.ShowLogs();
        return false;
    }
    if (!IfEveryStackIsNotEmpty(loggerStack) || !CheckStacks(loggerStack)) {
        loggerStack.ShowLogs();
        return false;
    }
    if (!IfEveryLayerIsNotEmpty(loggerLayer) || !CheckLayers(loggerLayer)) {
        loggerLayer.ShowLogs();
        return false;
    }
    if (!IfEveryRowIsNotEmpty(loggerRow) || !CheckRows(loggerRow)) {
        loggerRow.ShowLogs();
        return false;
    }

    if (!IfNullBinHasSmallestVolume() || !CheckCountProductInBinZero()) {
        std::cerr << "error in bin 0" << std::endl;
        return false;
    }

    return true;
}


BinChecker::BinChecker(
        const OutputBin& bin, 
        const std::vector<OutputStack>& stacksVector)
        : outputBin(bin)
        , outputStacks(stacksVector) {
}

bool BinChecker::CheckWeight() {
    return outputBin.GetWeight() <= outputBin.GetMaximumWeightAllowed();
}

bool BinChecker::IfStacksLayEntirelyInside() {
    Point3D binExtremity = outputBin.extremity;

    for (unsigned idx = 0; idx < outputBin.GetCountChildren(); ++idx) {
        Point3D currentExtremity = outputStacks[outputBin.GetChildIdByIndex(idx)].extremity;

        if ((currentExtremity.x > binExtremity.x) 
                || (currentExtremity.y > binExtremity.y) || (currentExtremity.z > binExtremity.z)) {
            return false;
        }
    }
    return true;
}

bool BinChecker::IfStacksNonOverlap() {
    for (unsigned idx = 0; idx < outputBin.GetCountChildren(); ++idx) {
        const Point2D lhsOrigin(outputStacks[outputBin.GetChildIdByIndex(idx)].origin.x,
            outputStacks[outputBin.GetChildIdByIndex(idx)].origin.y);
        const Point2D lhsExtremity(outputStacks[outputBin.GetChildIdByIndex(idx)].extremity.x,
            outputStacks[outputBin.GetChildIdByIndex(idx)].extremity.y);
        const PosedRectangle lhsRectangle(lhsOrigin, lhsExtremity);

        for (unsigned jnd = idx + 1; jnd < outputBin.GetCountChildren(); ++jnd) {
            const Point2D rhsOrigin(outputStacks[outputBin.GetChildIdByIndex(jnd)].origin.x,
                outputStacks[outputBin.GetChildIdByIndex(jnd)].origin.y);
            const Point2D rhsExtremity(outputStacks[outputBin.GetChildIdByIndex(jnd)].extremity.x,
                outputStacks[outputBin.GetChildIdByIndex(jnd)].extremity.y);
            const PosedRectangle rhsRectangle(rhsOrigin, rhsExtremity);

            if (lhsRectangle.IfHaveIntersection(rhsRectangle)) {
                return false;
            }
        }
    }

    return true;
}

StackChecker::StackChecker(
        const OutputStack& stack,
        const std::vector<OutputLayer>& layersVector,
        const std::vector<OutputRow>& rowsVector,
        const std::vector<OutputItem>& itemsVector,
        const Context& parameters)
        : outputStack(stack)
        , outputLayers(layersVector)
        , outputRows(rowsVector)
        , outputItems(itemsVector)
        , context(parameters) {
    for (unsigned idx = 0; idx < outputStack.GetCountChildren(); ++idx) {
        orderedChildrenIds.push_back(outputStack.GetChildIdByIndex(idx));
    }
    std::sort(orderedChildrenIds.begin(), orderedChildrenIds.end(), LayerWeightComparator(outputLayers));
}

bool StackChecker::IfLayersAreSortedByWeights() {
    if (orderedChildrenIds.size() == 0) {
        return true;
    }
    for (unsigned idx = 1; idx < orderedChildrenIds.size(); ++idx) {
        if (outputLayers[orderedChildrenIds[idx]].origin.z <= 
                outputLayers[orderedChildrenIds[idx - 1]].origin.z) {
            return false;
        }
    }
    return true;
}

bool StackChecker::IfStackIsBuiltCorrectly() {
    Point3D stackOrigin = outputStack.origin;
    Point3D stackExtremity = outputStack.extremity;

    if (IfLayersAreSortedByWeights()) {
        unsigned zExtremity = 0;
        unsigned xExtremity = 0;
        unsigned yExtremity = 0;

        for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {

            if ((outputLayers[orderedChildrenIds[idx]].origin.x != stackOrigin.x) ||
                    (outputLayers[orderedChildrenIds[idx]].origin.y != stackOrigin.y) ||
                    (outputLayers[orderedChildrenIds[idx]].origin.z != zExtremity)) {
                return false;
            }

            zExtremity = outputLayers[orderedChildrenIds[idx]].extremity.z;
            xExtremity = std::max(xExtremity, outputLayers[orderedChildrenIds[idx]].extremity.x);
            yExtremity = std::max(yExtremity, outputLayers[orderedChildrenIds[idx]].extremity.y);
        }
        if ((xExtremity != stackExtremity.x) || (yExtremity != stackExtremity.y) || (zExtremity != stackExtremity.z)) {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

bool StackChecker::IfLayersDimensionsAreEqual() {
    if (outputStack.GetCountChildren() <= 1) {
        return true;
    }

    unsigned maxLength = outputLayers[orderedChildrenIds[0]].extremity.y
        - outputLayers[orderedChildrenIds[0]].origin.y;
    unsigned maxWidth = outputLayers[orderedChildrenIds[0]].extremity.x
        - outputLayers[orderedChildrenIds[0]].origin.x;
    unsigned minLength = maxLength;
    unsigned minWidth = maxWidth;

    for (unsigned idx = 1; idx < orderedChildrenIds.size(); ++idx) {

        unsigned currLength = outputLayers[orderedChildrenIds[idx]].extremity.y
            - outputLayers[orderedChildrenIds[idx]].origin.y;
        unsigned currWidth = outputLayers[orderedChildrenIds[idx]].extremity.x
            - outputLayers[orderedChildrenIds[idx]].origin.x;

        maxLength = std::max(currLength, maxLength);
        maxWidth = std::max(currWidth, maxWidth);

        if ((idx + 1) != orderedChildrenIds.size()) {
            minLength = std::min(currLength, minLength);
            minWidth = std::min(currWidth, minWidth);
        }
    }

    if (static_cast<double>(maxLength) / static_cast<double>(minLength) >
            1.0 + context.layer_size_deviation_tolerance) {
        return false;
    }
    if (static_cast<double>(maxWidth) / static_cast<double>(minWidth) >
            1.0 + context.layer_size_deviation_tolerance) {
        return false;
    }

    return true;
}


bool StackChecker::CheckDensity() {
    double totalWeight = outputStack.GetWeight();
    double stackLength = static_cast<double>(outputStack.extremity.y) - static_cast<double>(outputStack.origin.y);
    double stackWidth = static_cast<double>(outputStack.extremity.x) - static_cast<double>(outputStack.origin.x);
    const double THOUSAND = 1000.0;
    double area = (stackLength / THOUSAND) * (stackWidth / THOUSAND);
    double density = totalWeight / area;
    return density <= context.maximum_density_allowed_kg || outputStack.GetIsMetal();
}


bool StackChecker::CheckWeightOnFundingLayerItems() {

    const OutputLayer& bottomLayer = outputLayers[orderedChildrenIds[0]];
    double weightBottomLessStack = outputStack.GetWeight() - bottomLayer.GetWeight();

    std::vector<unsigned> itemsAreas = GetAreasForItemsInBottomLayer();

    double bottomArea = 0;
    double maxItemArea = 0;
    for (unsigned idx = 0; idx < itemsAreas.size(); ++idx) {
        bottomArea += static_cast<double>(itemsAreas[idx]);
        maxItemArea = std::max(maxItemArea, static_cast<double>(itemsAreas[idx]));
    }
    const double EPS = 1e-5;
    bool satisfied = (maxItemArea / bottomArea) * weightBottomLessStack
        <= context.maximum_weigth_above_item_kg + EPS;
    bool haveMetalItems = outputStack.GetIsMetal();
    return (satisfied || haveMetalItems);
}

std::vector<unsigned> StackChecker::GetAreasForItemsInBottomLayer() {
    std::vector<unsigned> areas;

    const OutputLayer& bottomLayer = outputLayers[orderedChildrenIds[0]];
    for (unsigned idx = 0; idx < bottomLayer.GetCountChildren(); ++idx) {
        const OutputRow& row = outputRows[bottomLayer.GetChildIdByIndex(idx)];

        for (unsigned jnd = 0; jnd < row.GetCountChildren(); ++jnd) {
            const OutputItem& item = outputItems[row.GetChildIdByIndex(jnd)];
            areas.push_back((item.extremity.x - item.origin.x) * (item.extremity.y - item.origin.y));
        }
    }

    return areas;
}


bool StackChecker::CheckMetalPackages() {
    unsigned countMetalLayers = 0;
    unsigned countNonMetalLayers = 0;

    for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
        if (outputLayers[orderedChildrenIds[idx]].GetIsMetal()) {
            ++countMetalLayers;
        } else {
            ++countNonMetalLayers;
        }
    }

    if ((countMetalLayers != 0) && (countNonMetalLayers != 0)) {
        return false;
    }

    return outputStack.GetIsMetal() == (countMetalLayers > 0);
}


LayerChecker::LayerChecker(
        const OutputLayer& layer,
        const std::vector<OutputRow>& rowsVector,
        const Context& parameters)
        : outputLayer(layer)
        , outputRows(rowsVector)
        , context(parameters) {
    for (unsigned idx = 0; idx < outputLayer.GetCountChildren(); ++idx) {
        orderedChildrenIds.push_back(outputLayer.GetChildIdByIndex(idx));
    }

    std::sort(orderedChildrenIds.begin(), orderedChildrenIds.end(), RowOriginComparator(outputRows));
}

bool LayerChecker::CheckMaxRowsAllowed() {
    if (outputLayer.GetIsMetal()) {
        if (outputLayer.GetCountChildren() > 1) {
            return false;
        }
    }
    return outputLayer.GetCountChildren() <= context.max_number_of_rows_in_a_layer;
}

bool LayerChecker::IfAllRowsHaveSameHeight() {
    unsigned layerZorigin = outputLayer.origin.z;
    unsigned layerZextremity = outputLayer.extremity.z;

    for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
        if ((outputRows[orderedChildrenIds[idx]].extremity.z != layerZextremity) ||
                (outputRows[orderedChildrenIds[idx]].origin.z != layerZorigin)) {
            return false;
        }
    }
    return true;
}

bool LayerChecker::IfLayerIsBuiltCorrectly() {
    unsigned layerXorigin = outputLayer.origin.x;
    unsigned layerYorigin = outputLayer.origin.y;
    unsigned layerXetremity = outputLayer.extremity.x;
    unsigned layerYextremity = outputLayer.extremity.y;

    if (outputRows[orderedChildrenIds[0]].origin.x == outputRows[orderedChildrenIds[orderedChildrenIds.size() - 1]].origin.x) {
        unsigned yValue = layerYorigin;
        unsigned xMax = layerXorigin;

        for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx){
            if ((outputRows[orderedChildrenIds[idx]].origin.x != layerXorigin) ||
                    (outputRows[orderedChildrenIds[idx]].origin.y != yValue)) {
                return false;
            }

            yValue = outputRows[orderedChildrenIds[idx]].extremity.y;
            xMax = std::max(xMax, outputRows[orderedChildrenIds[idx]].extremity.x);
        }

        if (yValue != layerYextremity) {
            return false;
        }
        if (xMax != layerXetremity) {
            return false;
        }
    } else {
        unsigned xValue = layerXorigin;
        unsigned yMax = layerYorigin;

        for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
            if ((outputRows[orderedChildrenIds[idx]].origin.y != layerYorigin) ||
                (outputRows[orderedChildrenIds[idx]].origin.x != xValue)) {
                return false;
            }

            xValue = outputRows[orderedChildrenIds[idx]].extremity.x;
            yMax = std::max(yMax, outputRows[orderedChildrenIds[idx]].extremity.y);
        }

        if (xValue != layerXetremity) {
            return false;
        }
        if (yMax != layerYextremity) {
            return false;
        }
    }
    return true;
}

bool LayerChecker::IfRowsDimensionsAreEqual() {
    if (outputLayer.GetIsTop()) {
        return true;
    }

    unsigned minWidth = outputRows[orderedChildrenIds[0]].extremity.x
        - outputRows[orderedChildrenIds[0]].origin.x;
    unsigned minLength = outputRows[orderedChildrenIds[0]].extremity.y
        - outputRows[orderedChildrenIds[0]].origin.y;
    unsigned maxWidth = minWidth;
    unsigned maxLength = minLength;

    for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
        unsigned width = outputRows[orderedChildrenIds[idx]].extremity.x
            - outputRows[orderedChildrenIds[idx]].origin.x;
        unsigned length = outputRows[orderedChildrenIds[idx]].extremity.y
            - outputRows[orderedChildrenIds[idx]].origin.y;
        minWidth = std::min(width, minWidth);
        minLength = std::min(length, minLength);
        maxLength = std::max(length, maxLength);
        maxWidth = std::max(width, maxWidth);
    }

    if (static_cast<double>(maxLength) / static_cast<double>(minLength) >
            1.0 + context.row_size_deviation_tolerance) {
        return false;
    }
    if (static_cast<double>(maxWidth) / static_cast<double>(minWidth) >
            1.0 + context.row_size_deviation_tolerance) {
        return false;
    }
    return true;
}

bool LayerChecker::CheckMetalPackages(){
    unsigned countMetalRows = 0;
    unsigned countNonMetalRows = 0;

    for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
        if (outputRows[orderedChildrenIds[idx]].GetIsMetal()) {
            ++countMetalRows;
        } else {
            ++countNonMetalRows;
        }
    }

    if ((countMetalRows != 0) && (countNonMetalRows != 0)) {
        return false;
    }

    return outputLayer.GetIsMetal() == (countMetalRows > 0);
}


RowChecker::RowChecker(
        const OutputRow& row,
        const std::vector<OutputItem>& itemsVector,
        const Context& parameters)
        : outputRow(row)
        , outputItems(itemsVector)
        , context(parameters) {
    for (unsigned idx = 0; idx < outputRow.GetCountChildren(); ++idx) {
        orderedChildrenIds.push_back(outputRow.GetChildIdByIndex(idx));
    }
    std::sort(orderedChildrenIds.begin(), orderedChildrenIds.end(), ItemOriginComparator(outputItems));
}

bool RowChecker::CheckMaxItemsAllowed() {
    if (outputRow.GetIsMetal()) {
        if (outputRow.GetCountChildren() > 1) {
            return false;
        }
    }
    return outputRow.GetCountChildren() <= context.max_number_of_items_in_a_row;
}

bool RowChecker::IfRowIsBuiltCorrectly() {
    unsigned rowXorigin = outputRow.origin.x;
    unsigned rowYorigin = outputRow.origin.y;
    unsigned rowXextremity = outputRow.extremity.x;
    unsigned rowYextremity = outputRow.extremity.y;

    if (outputItems[orderedChildrenIds[0]].origin.x ==
            outputItems[orderedChildrenIds[orderedChildrenIds.size() - 1]].origin.x) {
        unsigned yValue = rowYorigin;
        unsigned xMax = rowXorigin;

        for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx){
            if ((outputItems[orderedChildrenIds[idx]].origin.x != rowXorigin) ||
                    (outputItems[orderedChildrenIds[idx]].origin.y != yValue)) {
                return false;
            }
            yValue = outputItems[orderedChildrenIds[idx]].extremity.y;
            xMax = std::max(xMax, outputItems[orderedChildrenIds[idx]].extremity.x);
        }

        if (yValue != rowYextremity) {
            return false;
        }
        if (xMax != rowXextremity) {
            return false;
        }
    } else {
        unsigned xValue = rowXorigin;
        unsigned yMax = rowYorigin;

        for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
            if ((outputItems[orderedChildrenIds[idx]].origin.y != rowYorigin) ||
                    (outputItems[orderedChildrenIds[idx]].origin.x != xValue)) {
                return false;
            }
            xValue = outputItems[orderedChildrenIds[idx]].extremity.x;
            yMax = std::max(yMax, outputItems[orderedChildrenIds[idx]].extremity.y);
        }

        if (xValue != rowXextremity) {
            return false;
        }
        if (yMax != rowYextremity) {
            return false;
        }
    }

    return true;
}

bool RowChecker::IfItemsDimensionsAreEqual() {
    unsigned minWidth = outputItems[orderedChildrenIds[0]].extremity.x
        - outputItems[orderedChildrenIds[0]].origin.x;
    unsigned minLength = outputItems[orderedChildrenIds[0]].extremity.y
        - outputItems[orderedChildrenIds[0]].origin.y;
    unsigned maxWidth = minWidth;
    unsigned maxLength = minLength;

    for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
        unsigned width = outputItems[orderedChildrenIds[idx]].extremity.x
            - outputItems[orderedChildrenIds[idx]].origin.x;
        unsigned length = outputItems[orderedChildrenIds[idx]].extremity.y
            - outputItems[orderedChildrenIds[idx]].origin.y;
        minWidth = std::min(width, minWidth);
        minLength = std::min(length, minLength);
        maxLength = std::max(length, maxLength);
        maxWidth = std::max(width, maxWidth);
    }

    if (static_cast<double>(maxLength) / static_cast<double>(minLength) >
            1.0 + context.item_size_deviation_tolerance) {
        return false;
    }
    if (static_cast<double>(maxWidth) / static_cast<double>(minWidth) >
            1.0 + context.item_size_deviation_tolerance) {
        return false;
    }
    return true;
}

bool RowChecker::IfItemsPositionCorrectly() {
    for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
        const OutputItem& item = outputItems[orderedChildrenIds[idx]];
        if (item.GetPosition() == Position::BOTH) {
            continue;
        }
        if ((item.GetPosition() == Position::LENGTHWISE) &&
                (item.extremity.x - item.origin.x < item.extremity.y - item.origin.y)) {
            return false;
        }
        if ((item.GetPosition() == Position::WIDTHWISE) &&
                (item.extremity.x - item.origin.x > item.extremity.y - item.origin.y)) {
            return false;
        }
    }
    return true;
}

bool RowChecker::IfAllItemsHaveSameHeight() {
    unsigned rowZorigin = outputRow.origin.z;
    unsigned rowZextremity = outputRow.extremity.z;

    for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
        if ((outputItems[orderedChildrenIds[idx]].extremity.z != rowZextremity) ||
                (outputItems[orderedChildrenIds[idx]].origin.z != rowZorigin)) {
            return false;
        }
    }
    return true;
}

bool RowChecker::CheckMetalPackages() {
    unsigned countMetalItems = 0;
    unsigned countNonMetalItems = 0;

    for (unsigned idx = 0; idx < orderedChildrenIds.size(); ++idx) {
        if (outputItems[orderedChildrenIds[idx]].GetIsMetal()) {
            ++countMetalItems;
        } else {
            ++countNonMetalItems;
        }
    }

    if ((countMetalItems != 0) && (countNonMetalItems != 0)) {
        return false;
    }

    return outputRow.GetIsMetal() == (countMetalItems > 0);
}

} // namespace checker
} // namespace esicup
