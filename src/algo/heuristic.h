/**
 * @file heuristic.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines and implements HeuristicPacker - an algorithm to load
 * trucks/bin given input data. 
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 * 
 */
#pragma once 

#include "interfaces.h"
#include "bridge.h"
#include "solvers.h"
#include "fillers.h"
#include "../output.h"
#include "../input.h"

#include <algorithm>
#include <memory>
#include <vector>

namespace esicup {
namespace algo {

using esicup::common::Item;
using esicup::common::Bin;
using esicup::common::Context;
using esicup::common::Material;
using esicup::common::OutputData;
using esicup::common::InputData;


/**
 * @brief HeuristicPacker is the algorithm entry point and prepares
 * loading output from scratch. HeuristicPacker prepares internal data
 * structures and delegates the task to the member solver. More precisely,
 * according to observations, the most effective is SortedCompositeSolver.
 */
class HeuristicPacker {
public:
    /**
     * @brief Constructor for Heuristic Packer.
     */
    HeuristicPacker() : solver(new SortedCompositeSolver) {}
    /**
     * @brief Destructor for Heuristic Packer.
     */
    virtual ~HeuristicPacker() {}
    /**
     * @brief Creates packing based on InputData. 
     * 
     * @param inputData The InputData information to start with.
     * @return Packing for output.
     */
    OutputData Process(const InputData& inputData) {
        PrepareData(inputData);
        DoFilling();
        FillOutputData();
        return outputData;
    }

private:
    /**
     * @brief Prepares internal structures.
     * 
     * @param inputData The InputData information to start with.
     */
    void PrepareData(const InputData& inputData) {
        bins = inputData.bins;
        context = inputData.context;
        itemProxies.reserve(inputData.items.size());
        for (const auto& item : inputData.items) {
            itemProxies.push_back(item);
        }

        DivideItemsOnGroups();
    }
    /**
     * @brief Creates groups of the same items (and stores 
     * the result in internal data structures).
     */
    void DivideItemsOnGroups() {
        if (itemProxies.empty()) { 
            return; 
        }
        std::sort(std::begin(itemProxies), std::end(itemProxies),
            [](const ItemProxy& lhs, const ItemProxy& rhs) -> bool {
                return lhs.IsLess(rhs);
            }
        );
        groups.push_back(std::vector<ItemProxy>());
        groups[0].push_back(itemProxies.front());
        for (size_t idx = 1; idx < itemProxies.size(); ++idx) {
            const auto& item = itemProxies[idx];
            if (!item.IsEqual(itemProxies[idx - 1])) {
                groups.push_back(std::vector<ItemProxy>());
            }
            groups.back().push_back(item);
        }
    }
    /**
     * @brief Packs bins (trucks) with items when all internal 
     * data structures are properly initialized.
     */
    void DoFilling() {
        filledBinProxies = solver->Solve(groups, bins, context);
    }
    /**
     * @brief Prepares OutputData, an object that can be evaluated 
     * and processed further in the non-algorithmic project part.
     */
    void FillOutputData() {
        outputData = ConvertToOutputData(filledBinProxies, context);
    }

private:
    /// @brief The vector with bins/trucs that can be used for packing.
    std::vector<Bin> bins;
    /// @brief The vector with available item proxies.
    std::vector<ItemProxy> itemProxies;
    /// @brief Problem context to satisfy and verify constraints.
    Context context;

    /// @brief Groups with the same items.
    std::vector< std::vector<ItemProxy> > groups;
    /// @brief Vector with filled bin proxies.
    std::vector< FilledBinProxy > filledBinProxies;
    /// @brief The resulting packing in format friendly outside algo scope.
    OutputData outputData;

    /// @brief The solver to use for packing.
    std::unique_ptr<ISolver> solver;
};


} // namespace algo
} // namespace esicup