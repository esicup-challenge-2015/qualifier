/// Implementation for methods mentioned in bridge.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "bridge.h"

namespace esicup {
namespace algo {

using esicup::common::OutputData;
using esicup::common::OutputStack;
using esicup::geometry::Point3D;


bool CheckBin(const Context& context, const FilledBinProxy& binProxy) {
    std::map<std::string, int> binProductCount;
    for (const auto& stack : binProxy.stackProxies) {
        for (const auto& itemProxy : stack.items) {
            binProductCount[itemProxy.item.productId] += 1;
        }
    }
    const auto& fullProductCount = context.productCount;
    for (const auto& product : binProductCount) {
        auto observedCount = product.second;
        auto acceptableCount = fullProductCount.at(product.first);
        if (observedCount > acceptableCount * context.tolerance_item_product_demands) {
            return false;
        }
    }
    return true;
}

void AddZeroBin(const Context& context, std::vector<FilledBinProxy>* filledBinProxies) {
    if (!filledBinProxies->size()) { return; }
    sort(std::begin(*filledBinProxies), std::end(*filledBinProxies),
            [] (const FilledBinProxy& lhs, const FilledBinProxy& rhs) -> bool {
            return (lhs.stackVolume < rhs.stackVolume);
    });
    if (!CheckBin(context, filledBinProxies->at(0))) {
        auto zeroBin = filledBinProxies->back();
        zeroBin.stackProxies.clear();
        zeroBin.CalculateParameters();
        FilledBinProxy headerBin = filledBinProxies->at(0);
        filledBinProxies->front() = zeroBin;
        (*filledBinProxies).push_back(headerBin);
    }
}

OutputData ConvertToOutputData(
        const std::vector<FilledBinProxy>& initFilledBinProxies, const Context& context) {
    auto filledBinProxies = initFilledBinProxies;
    for (auto& binProxy : filledBinProxies) {
        binProxy.CalculateParameters();
    }
    AddZeroBin(context, &filledBinProxies);
    unsigned numberOfItems = 0;
    for (const auto& binProxy : filledBinProxies) {
        for (const auto& stack : binProxy.stackProxies) {
            numberOfItems += stack.items.size();
        }
    }

    OutputData outputData = OutputData();
    outputData.context = context;
    outputData.items.resize(numberOfItems);
    outputData.rows.resize(numberOfItems);
    outputData.layers.resize(numberOfItems);
    outputData.bins.resize(filledBinProxies.size());

    for (unsigned binId = 0; binId < filledBinProxies.size(); ++binId) {
        const auto& binProxy = filledBinProxies[binId];
        outputData.bins[binId].type = binProxy.bin.binType;
        outputData.bins[binId].id = binId;
        outputData.bins[binId].idParent = 0;
        outputData.bins[binId].origin = Point3D(0, 0, 0);
        outputData.bins[binId].extremity = Point3D(
            binProxy.bin.length, binProxy.bin.width, binProxy.bin.height);

        for (const auto& stackProxy : binProxy.stackProxies) {
            OutputStack outputStack;
            unsigned stackId = outputData.stacks.size();
            outputStack.id = stackId;
            outputStack.idParent = binId;
            outputStack.origin = stackProxy.origin;
            outputStack.extremity = stackProxy.extremity;
            outputData.stacks.push_back(outputStack);

            for (const auto& itemProxy : stackProxy.items) {
                unsigned itemId = itemProxy.item.itemId;
                outputData.items[itemId].id = itemId;
                outputData.items[itemId].idParent = itemId;
                outputData.items[itemId].origin = itemProxy.origin;
                outputData.items[itemId].extremity = itemProxy.extremity;

                outputData.rows[itemId].id = itemId;
                outputData.rows[itemId].idParent = itemId;
                outputData.rows[itemId].origin = itemProxy.origin;
                outputData.rows[itemId].extremity = itemProxy.extremity;

                outputData.layers[itemId].id = itemId;
                outputData.layers[itemId].idParent = stackId;
                outputData.layers[itemId].origin = itemProxy.origin;
                outputData.layers[itemId].extremity = itemProxy.extremity;
            }
        }
    }

    return outputData;
}

} // namespace algo
} // namespace esicup

