/// Implementation for API and methods declared at fillers.h
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "interfaces.h"
#include "fillers.h"

#include <algorithm>
#include <limits>

namespace esicup {
namespace algo {

using esicup::common::Material;
using esicup::geometry::Point3D;
using esicup::geometry::PosedRectangle;
using esicup::geometry::Point2D;


FilledBinProxy XYFiller::FillBin(
        const Bin& bin, std::vector<std::vector<ItemProxy>>& groups, const Context& context) {
    FilledBinProxy resultBinProxy;
    resultBinProxy.bin = bin;
    unsigned rowLowerY = 0; // last row lower bound
    unsigned rowUpperY = 0; // last row upper bound
    unsigned rowRightX = 0; // last row right bound
    double weightSum = 0.;
    for (int currentGroupNumber = static_cast<int>(groups.size()) - 1; currentGroupNumber >= 0; ) {

        auto& group = groups[currentGroupNumber];
        if (!group.size()) {
            --currentGroupNumber;
            continue;
        }

        auto& itemProxy = group.back();
        if (weightSum + itemProxy.item.weight <= resultBinProxy.bin.maximumWeightAllowed) {
            if (TryPutToAllStacks(itemProxy, context, &resultBinProxy)) {
                group.pop_back();
                weightSum += itemProxy.item.weight;
                continue;
            }
        }

        const double EPS = 1e-8;
        unsigned maxCount = CountHowManyCanPut(group, bin, context);
        maxCount = std::min(maxCount, static_cast<unsigned>(
                    EPS + (bin.maximumWeightAllowed - weightSum) / itemProxy.item.weight));
        if (!maxCount) {
            if (!canSkip) {
                break;
            }
            --currentGroupNumber;
            continue;
        }
        weightSum += maxCount * itemProxy.item.weight;
        unsigned width = 0;
        unsigned length = 0;
        unsigned height = itemProxy.item.height;

        SpecifyPosition(itemProxy, bin, length, width);

        unsigned currentX = rowRightX;
        unsigned currentY = rowLowerY;
        if (currentX + length > bin.length) {
            currentX = 0;
            currentY = rowUpperY;
        }
        if (currentX + length > bin.length || currentY + width > bin.width) {
            if (!canSkip) {
                break;
            }
            --currentGroupNumber;
            continue;
        }
        rowLowerY = currentY;
        rowUpperY = std::max(rowUpperY, rowLowerY + width);
        rowRightX = currentX + length;
        StackProxy stackProxy;
        stackProxy.origin = Point3D(currentX, currentY, 0);
        stackProxy.extremity = Point3D(currentX + length, currentY + width, height * maxCount);
        while (maxCount--) {
            auto currentItemProxy = group.back();
            currentItemProxy.origin = stackProxy.origin;
            currentItemProxy.origin.z = maxCount * height;
            currentItemProxy.extremity = stackProxy.extremity;
            currentItemProxy.extremity.z = height * (maxCount + 1);
            stackProxy.items.push_back(currentItemProxy);
            group.pop_back();
        }
        resultBinProxy.stackProxies.push_back(stackProxy);
    }
    std::vector<std::vector<ItemProxy>> updatedGroups;
    for (auto& member : groups) {
        if (member.size()) {
            updatedGroups.push_back(member);
        }
    }
    groups = updatedGroups;
    resultBinProxy.CalculateParameters();
    return resultBinProxy;
}

std::string XYFiller::GetName() const {
        std::string methodName = "Filler: XY. ";
        std::string skipOption = "";
        if (canSkip) {
            skipOption = "Skip Allowed.";
        } else {
            skipOption = "No Skip.";
        }
        return methodName + skipOption;
}

FilledBinProxy YXFiller::FillBin(const Bin& bin, std::vector<std::vector<ItemProxy>>& groups, const Context& context) {
    FilledBinProxy resultBinProxy;
    resultBinProxy.bin = bin;
    unsigned columnLeftX = 0; // last col left bound
    unsigned columnRightX = 0; // last col right bound
    unsigned columnLowerY = 0;  // last col lower bound
    double weightSum = 0.;
    for (int currentGroupNumber = static_cast<int>(groups.size()) - 1; currentGroupNumber >= 0; ) {
        auto& group = groups[currentGroupNumber];
        auto& itemProxy = group.back();
        if (!group.size()) {
            --currentGroupNumber;
            continue;
        }
        if (weightSum + itemProxy.item.weight <= resultBinProxy.bin.maximumWeightAllowed) {
            if (TryPutToAllStacks(itemProxy, context, &resultBinProxy)) {
                group.pop_back();
                weightSum += itemProxy.item.weight;
                continue;
            }
        }

        const double EPS = 1e-8;
        unsigned maxCount = CountHowManyCanPut(group, bin, context);
        maxCount = std::min(maxCount, static_cast<unsigned>(
                    EPS + (bin.maximumWeightAllowed - weightSum) / itemProxy.item.weight));
        if (!maxCount) {
            if (!canSkip) {
                break;
            }
            --currentGroupNumber;
            continue;
        }
        weightSum += maxCount * itemProxy.item.weight;
        unsigned width = 0;
        unsigned length = 0;
        unsigned height = itemProxy.item.height;
        SpecifyPosition(itemProxy, bin, length, width);
        unsigned currentX = columnLeftX;
        unsigned currentY = columnLowerY;
        if (currentY + width > bin.width) {
            currentX = columnRightX;
            currentY = 0;
        }
        if (currentX + length > bin.length || currentY + width > bin.width) {
            if (!canSkip) {
                break;
            }
            --currentGroupNumber;
            continue;
        }
        columnLeftX = currentX;
        columnRightX = std::max(columnRightX, columnLeftX + length);
        columnLowerY = currentY + width;

        StackProxy stackProxy;
        stackProxy.origin = Point3D(currentX, currentY, 0);
        stackProxy.extremity = Point3D(currentX + length, currentY + width, height * maxCount);
        while (maxCount--) {
            auto currentItemProxy = group.back();
            currentItemProxy.origin = stackProxy.origin;
            currentItemProxy.origin.z = maxCount * height;
            currentItemProxy.extremity = stackProxy.extremity;
            currentItemProxy.extremity.z = height * (maxCount + 1);
            stackProxy.items.push_back(currentItemProxy);
            group.pop_back();
        }
        resultBinProxy.stackProxies.push_back(stackProxy);
    }

    std::vector<std::vector<ItemProxy>> updatedGroups;
    for (auto& member : groups) {
        if (member.size()) {
            updatedGroups.push_back(member);
        }
    }
    groups = updatedGroups;
    resultBinProxy.CalculateParameters();
    return resultBinProxy;
}

std::string YXFiller::GetName() const {
    std::string methodName = "Filler: YX. ";
    std::string skipOption = "";
    if (canSkip) {
        skipOption = "Skip Allowed.";
    } else {
        skipOption = "No Skip.";
    }
    return methodName + skipOption;
}

FilledBinProxy RoundFiller::FillBin(
        const Bin& bin,
        std::vector<std::vector<ItemProxy>>& groups,
        const Context& context) {
    if (centralize) {
        center = Point2D(bin.length / 2, bin.width / 2);
    }
    FilledBinProxy targetBinProxy;
    targetBinProxy.bin = bin;
    if (targetBinProxy.bin.length < center.x || targetBinProxy.bin.width < center.y) {
        return targetBinProxy;
    }
    std::vector<PosedRectangle> fixedRectangles;
    double weightSum = 0.0;
    for (size_t idGroup = 0; idGroup < groups.size();) {
        if (!groups[idGroup].size()) {
            ++idGroup;
            continue;
        }
        auto& currentGroup = groups[idGroup];
        auto& itemProxy = currentGroup.back();

        if (weightSum + itemProxy.item.weight <= targetBinProxy.bin.maximumWeightAllowed) {
            if (TryPutToAllStacks(itemProxy, context, &targetBinProxy)) {
                currentGroup.pop_back();
                weightSum += itemProxy.item.weight;
                continue;
            }
        }
        std::vector<Point2D> pointCandidates;
        pointCandidates.push_back(center);
        pointCandidates.push_back(Point2D(0, 0));
        pointCandidates.push_back(Point2D(bin.length, 0));
        pointCandidates.push_back(Point2D(bin.width, 0));
        pointCandidates.push_back(Point2D(bin.width, bin.length));
        for (auto position : fixedRectangles) {
            pointCandidates.push_back(position.origin);
            pointCandidates.push_back(position.origin.GetTranslated(
                        Point2D(position.rectangle.xLen, 0)));
            pointCandidates.push_back(position.origin.GetTranslated(
                        Point2D(0, position.rectangle.yLen)));
            pointCandidates.push_back(position.origin.GetTranslated(
                        Point2D(position.rectangle.xLen, position.rectangle.yLen)));
        }
        sort(begin(pointCandidates), end(pointCandidates),
                [&](const Point2D& first, const Point2D& second) -> bool {
                    long long dx1 = static_cast<long long>(first.x)
                                  - static_cast<long long>(center.x);
                    long long dy1 = static_cast<long long>(first.y)
                                  - static_cast<long long>(center.y);
                    long long dx2 = static_cast<long long>(second.x)
                                  - static_cast<long long>(center.x);
                    long long dy2 = static_cast<long long>(second.y)
                                  - static_cast<long long>(center.y);
                    return ((dx1 * dx1 + dy1 * dy1) < (dx2 * dx2 + dy2 * dy2));
                });
        bool foundPosition = false;
        PosedRectangle resultRectangle;

        for (auto point : pointCandidates) {
            if (foundPosition) {
                break;
            }
            std::vector<PosedRectangle> candidates;
            if (itemProxy.item.canBeWidthwise) {
                Point2D oppositeForward = point.GetTranslated(
                        Point2D(itemProxy.item.width, itemProxy.item.length));
                candidates.push_back(PosedRectangle(point, oppositeForward));
                if (point.x >= itemProxy.item.width
                    && point.y >= itemProxy.item.length) {
                    Point2D oppositeBackward = Point2D(
                            point.x - itemProxy.item.width, point.y - itemProxy.item.length);
                    candidates.push_back(PosedRectangle(point, oppositeBackward));
                }
            }
            if (itemProxy.item.canBeLengthwise) {
                Point2D oppositeForward = point.GetTranslated(
                        Point2D(itemProxy.item.length, itemProxy.item.width));
                candidates.push_back(
                        PosedRectangle(point, oppositeForward));
                if (point.y >= itemProxy.item.width
                    && point.x >= itemProxy.item.length) {
                    Point2D oppositeBackward = Point2D(
                            point.x - itemProxy.item.length, point.y - itemProxy.item.width);
                    candidates.push_back(PosedRectangle(point, oppositeBackward));
                }
            }
            for (auto rectangle : candidates) {
                if (rectangle.origin.x + rectangle.rectangle.xLen <= bin.length
                        && rectangle.origin.y + rectangle.rectangle.yLen <= bin.width
                        && IfCanPutRectangle(fixedRectangles, rectangle)) {
                    foundPosition = true;
                    resultRectangle = rectangle;
                    break;
                }
            }
        }
        unsigned maxCount = CountHowManyCanPut(currentGroup, bin, context);
        const double EPS = 1e-8;
        maxCount = std::min(maxCount, static_cast<unsigned>(
                    EPS + (bin.maximumWeightAllowed - weightSum) / itemProxy.item.weight));

        if (!foundPosition || !maxCount) {
            ++idGroup;
            continue;
        }

        fixedRectangles.push_back(resultRectangle);
        weightSum += maxCount * itemProxy.item.weight;
        StackProxy stackProxy;
        stackProxy.origin = Point3D(resultRectangle.origin.x, resultRectangle.origin.y, 0);
        stackProxy.extremity = stackProxy.origin.GetTranslated(Point3D(
                    resultRectangle.rectangle.xLen,
                    resultRectangle.rectangle.yLen,
                    itemProxy.item.height * maxCount));

        unsigned currentHeight = 0;
        while (maxCount--) {
            auto currentItemProxy = currentGroup.back();
            currentItemProxy.origin = stackProxy.origin;
            currentItemProxy.origin.z = currentHeight;
            currentHeight += currentItemProxy.item.height;
            currentItemProxy.extremity = stackProxy.extremity;
            currentItemProxy.extremity.z = currentHeight;
            stackProxy.items.push_back(currentItemProxy);
            currentGroup.pop_back();
        }
        targetBinProxy.stackProxies.push_back(stackProxy);
    }
    auto previousGroups = groups;
    groups.clear();
    for (auto entry : previousGroups) {
        if (entry.size()) {
            groups.push_back(entry);
        }
    }
    targetBinProxy.CalculateParameters();
    return targetBinProxy;
}


std::string RoundFiller::GetName() const {
    std::string methodName = "Filler: Round. ";
    std::string option = "";
    if (centralize) {
        option = "Centralization applied.";
    } else {
        option = "No centralization.";
    }
    return methodName + option;
}


bool IfCanPutRectangle(
        const std::vector<PosedRectangle>& fixedRectangles,
        PosedRectangle& currentRectangle) {
    for (const auto& fixed : fixedRectangles) {
        if (fixed.IfHaveIntersection(currentRectangle))  {
            return false;
        }
    }
    return true;
}

bool TryPutToAllStacks(
        const ItemProxy& itemProxy,
        const Context& context,
        FilledBinProxy* binProxy) {
    for (auto& stackProxy : binProxy->stackProxies) {
        StackProxy updatedStackProxy;
        if (IfCanAdd(itemProxy, *binProxy, context, stackProxy, &updatedStackProxy)) {
            stackProxy = updatedStackProxy;
            return true;
        }
    }
    return false;
}

unsigned CountHowManyCanPut(
        const std::vector<ItemProxy>& group, 
        const Bin& bin, 
        const Context& context) {
    const auto& itemProxy = group.back();
    unsigned byContext = itemProxy.GetMaxNumberInStack(context);
    unsigned byGroup = std::min(bin.height / itemProxy.item.height, 
                                static_cast<unsigned>(group.size()));
    return std::min(byContext, byGroup);
}

void SpecifyPosition(
        const ItemProxy& itemProxy, 
        const Bin& bin, 
        unsigned& length, 
        unsigned& width) {
    if (!itemProxy.item.canBeLengthwise) {
        length = itemProxy.item.width;
        width = itemProxy.item.length;
    } else if (!itemProxy.item.canBeWidthwise) {
        length = itemProxy.item.length;
        width = itemProxy.item.width;
    } else if (bin.length > bin.width) {
        length = std::max(itemProxy.item.length, itemProxy.item.width);
        width = std::min(itemProxy.item.width, itemProxy.item.length);
    } else {
        width = std::max(itemProxy.item.length, itemProxy.item.width);
        length = std::min(itemProxy.item.width, itemProxy.item.length);
    }
}

bool IfCanAdd(
        const ItemProxy& itemProxy,
        const FilledBinProxy& binProxy,
        const Context& context,
        const StackProxy& stackProxy,
        StackProxy* resultStackProxy) {

    bool allOk = true;
    // Initializing parameters of resultStack.
    resultStackProxy->origin = stackProxy.origin;
    resultStackProxy->extremity = stackProxy.extremity;

    unsigned diffx = stackProxy.extremity.x - stackProxy.origin.x;
    unsigned diffy = stackProxy.extremity.y - stackProxy.origin.y;

    // Deciding on how to put this item into the stack (position).
    unsigned xDelta = 0, yDelta = 0;
    if (!itemProxy.item.canBeWidthwise) {
        xDelta = itemProxy.item.length;
        yDelta = itemProxy.item.width;
    } else if (!itemProxy.item.canBeLengthwise) {
        xDelta = itemProxy.item.width;
        yDelta = itemProxy.item.length;
    } else {
        if (diffx > diffy) {
            xDelta = itemProxy.item.length;
            yDelta = itemProxy.item.width;
        } else {
            xDelta = itemProxy.item.width;
            yDelta = itemProxy.item.length;
        }
    }

    // Check that we do not change envelope.
    if (xDelta > diffx || yDelta > diffy) {
        allOk = false;
    }
    auto stackItemProxies = stackProxy.items;
    stackItemProxies.push_back(itemProxy);
    stackItemProxies.back().origin = stackProxy.origin;
    stackItemProxies.back().extremity.x = stackProxy.origin.x + xDelta;
    stackItemProxies.back().extremity.y = stackProxy.origin.y + yDelta;
    double allWeight = 0.;

    // Layers in stack are sorted in decreasing order from the bottom to the top.
    // Here stackItems[0] is the bottom layer.
    sort(std::begin(stackItemProxies), std::end(stackItemProxies),
            [](const ItemProxy& first, const ItemProxy& second) -> bool {
                return first.item.weight > second.item.weight;
        });

    bool haveMetalItems = false;
    bool haveNonMetalItems = false;

    unsigned allHeight = 0;
    for (auto& itemProxy : stackItemProxies) {
        if (itemProxy.item.material == Material::METAL) {
            haveMetalItems = true;
        } else {
            haveNonMetalItems = true;
        }
        itemProxy.origin.z = allHeight;
        allHeight += itemProxy.item.height;
        itemProxy.extremity.z = allHeight;
        allWeight += itemProxy.item.weight;
    }

    // Check for (C12)
    if (haveMetalItems && haveNonMetalItems) {
        allOk = false;
    }

    // Set z coordinates for result stack.
    resultStackProxy->origin.z = 0;
    resultStackProxy->extremity.z = allHeight;

    // Check that we fit bin weight(C2) and bin height(C9).
    if (allHeight > binProxy.bin.height
            || allWeight > binProxy.bin.maximumWeightAllowed) {
        allOk = false;
    }

    // Check that we fit density constraint (C13).
    unsigned long long area = static_cast<unsigned long long>(stackItemProxies[0].item.length)
                            * static_cast<unsigned long long>(stackItemProxies[0].item.width);
    const double THOUSAND = 1000.;
    double density = allWeight * THOUSAND * THOUSAND / area;
    if (haveNonMetalItems && density > context.maximum_density_allowed_kg) {
        allOk = false;
    }

    // Check that we fit constraint (C29).
    if (haveNonMetalItems && allWeight - stackItemProxies[0].item.weight > context.maximum_weigth_above_item_kg) {
        allOk = false;
    }

    // Check that we fit layer dimension ratio constraint.
    const unsigned INFINITY = std::numeric_limits<unsigned>::max();
    const double EPS = 1e-5;
    unsigned maxXDelta = 1, maxYDelta = 1, minXDelta = INFINITY, minYDelta = INFINITY;
    for (size_t index = 0; index < stackItemProxies.size(); ++index) {
        maxXDelta = std::max(
                stackItemProxies[index].extremity.x - stackItemProxies[index].origin.x, maxXDelta);
        maxYDelta = std::max(
                stackItemProxies[index].extremity.y - stackItemProxies[index].origin.y, maxYDelta);
        if (index + 1 != stackItemProxies.size()) {
            minXDelta = std::min(
                    stackItemProxies[index].extremity.x - stackItemProxies[index].origin.x,  minXDelta);
            minYDelta = std::min(
                    stackItemProxies[index].extremity.y - stackItemProxies[index].origin.y, minYDelta);
        }
    }
    double size_deviation = std::max(
            static_cast<double>(maxXDelta) / static_cast<double>(minXDelta),
            static_cast<double>(maxYDelta) / static_cast<double>(minYDelta));

    if (size_deviation + EPS >  1. + context.layer_size_deviation_tolerance) {
        allOk = false;
    }
    if (allOk) {
        resultStackProxy->items = stackItemProxies;
    }
    return allOk;
}

} // namespace algo
} // namespace esicup

