/// IMplements API and methods defined in solvers.h.
///
/// Author: Aliaksandr Nekrashevich
/// Email: aliaksandr.nekrashevich@queensu.ca
/// (c) Belarusian State University, 2015
/// (c) Smith School of Business, 2024

#include "interfaces.h"
#include "bridge.h"
#include "solvers.h"

#include <cstdlib>
#include <algorithm>
#include <cassert>


namespace esicup {
namespace algo {

using esicup::checker::Objective;
using esicup::checker::ObjectiveCalculator;
using esicup::common::OutputData;


std::vector<FilledBinProxy> StraightSolver::Solve(
        std::vector<std::vector<ItemProxy>> groups,
        const std::vector<Bin>& bins,
        const Context& context) {
    std::vector<FilledBinProxy> allocation;

    unsigned binNumber = 0;
    while (!groups.empty()) {
        binNumber = (binNumber + 1) % bins.size();
        allocation.push_back(filler->FillBin(bins[binNumber], groups, context));
    }

    return allocation;
}

std::vector<FilledBinProxy> LocallyOptimalFillerIterativeBinByVolumeChooserSolver::Solve(
        std::vector<std::vector<ItemProxy>> groups, const std::vector<Bin>& bins, const Context& context) {
    std::vector<FilledBinProxy> allocation;
    unsigned binNumber = 1;
    while (!groups.empty()) {
        binNumber = (binNumber + 1) % bins.size();
        std::vector<std::vector<ItemProxy>> selectedGroups;
        FilledBinProxy selectedBin;
        bool updated = false;
        for (auto& fillerMethod : fillers) {
            std::vector<std::vector<ItemProxy>> groupInstance(groups);
            FilledBinProxy candidateBin = fillerMethod->FillBin(
                    bins[binNumber], groupInstance, context);
            if (!updated || candidateBin.usefulVolumeRatio >  selectedBin.usefulVolumeRatio) {
                updated = true;
                selectedBin = candidateBin;
                selectedGroups = groupInstance;
            }
        }
        allocation.push_back(selectedBin);
        groups = selectedGroups;
    }
    return allocation;
}

std::vector<FilledBinProxy> LocallyOptimalFillerAndBinByVolumeChooserSolver::Solve(
        std::vector<std::vector<ItemProxy>> groups, const std::vector<Bin>& bins, const Context& context) {
    std::vector<FilledBinProxy> allocation;
    while (!groups.empty()) {
        std::vector<std::vector<ItemProxy>> selectedGroups;
        FilledBinProxy selectedBin;
        bool updated = false;
        for (unsigned binNumber = 0; binNumber < bins.size(); ++binNumber) {
            for (auto& fillerMethod : fillers) {
                std::vector<std::vector<ItemProxy>> groupsInstance(groups);
                FilledBinProxy candidateBin = fillerMethod->FillBin(
                        bins[binNumber], groupsInstance, context);

                if (!updated
                        || candidateBin.usefulVolumeRatio > selectedBin.usefulVolumeRatio) {
                    updated = true;
                    selectedBin = candidateBin;
                    selectedGroups = groupsInstance;
                }
            }
        }
        allocation.push_back(selectedBin);
        groups = selectedGroups;
    }

    return allocation;
}

std::vector<FilledBinProxy> LocallyOptimalFillerAndBinByAreaChooserSolver::Solve(
        std::vector<std::vector<ItemProxy>> groups, const std::vector<Bin>& bins, const Context& context) {
    std::vector<FilledBinProxy> allocation;
    while (!groups.empty()) {
        std::vector<std::vector<ItemProxy>> selectedGroups;
        FilledBinProxy selectedBin;
        bool updated = false;
        for (unsigned binNumber = 0; binNumber < bins.size(); ++binNumber) {
            for (auto& fillerMethod : fillers) {
                std::vector<std::vector<ItemProxy>> groupsInstance(groups);
                FilledBinProxy candidateBin = fillerMethod->FillBin(
                        bins[binNumber], groupsInstance, context);

                if (!updated || candidateBin.usefulAreaRatio > selectedBin.usefulAreaRatio) {
                    updated = true;
                    selectedBin = candidateBin;
                    selectedGroups = groupsInstance;
                }
            }
        }
        allocation.push_back(selectedBin);
        groups = selectedGroups;
    }
    return allocation;
}

bool UpdateBestAllocation(
        const std::vector<std::vector<ItemProxy>>& groups,
        const std::vector<Bin>& bins,
        const Context& context,
        const std::unique_ptr<ISolver>& solver,
        std::vector<FilledBinProxy>* selectedAllocation,
        Objective* bestScore,
        bool* updated) {
    std::vector<FilledBinProxy> candidateAllocation = solver->Solve(groups, bins, context);
    OutputData converted = ConvertToOutputData(candidateAllocation, context);
    ObjectiveCalculator scorer(converted);
    scorer.Compute();
    Objective candidateScore = scorer.GetObjective();
    if (!(*updated) || candidateScore < *bestScore) {
        *updated = true;
        *selectedAllocation = candidateAllocation;
        *bestScore = candidateScore;
        return true;
    }
    return false;
}

void UpdateByApplyingSortings(
        std::vector<std::vector<ItemProxy>> groups,
        const std::vector<Bin>& bins,
        const Context& context,
        const std::unique_ptr<ISolver>& solver,
        std::vector<FilledBinProxy>* selectedAllocation,
        Objective* bestScore,
        bool* updated) {
    // Sorting by sum height.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs) -> bool {
                unsigned long long valueLHS
                    = static_cast<unsigned long long>(lhs.back().item.height)
                    * static_cast<unsigned long long>(lhs.size());
                unsigned long long valueRHS
                    = static_cast<unsigned long long>(rhs.back().item.height)
                    * static_cast<unsigned long long>(rhs.size());

                return valueLHS < valueRHS;
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by item height.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs)  -> bool {
                return lhs.back().item.height < rhs.back().item.height;
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by sum width.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs)  -> bool {
                unsigned long long valueLHS
                    = static_cast<unsigned long long>(lhs.back().item.width)
                    * static_cast<unsigned long long>(lhs.size());
                unsigned long long valueRHS
                    = static_cast<unsigned long long>(rhs.back().item.width)
                    * static_cast<unsigned long long>(rhs.size());
                return valueLHS < valueRHS;
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by width.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs) -> bool {
                return lhs.back().item.width < rhs.back().item.width;
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by sum length.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs) -> bool {
                return lhs.back().item.length * lhs.size() < rhs.back().item.length * rhs.size();
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by length.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs) -> bool {
                return lhs.back().item.length < rhs.back().item.length;
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by sum weight.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs) -> bool {
                return lhs.back().item.weight * lhs.size() < rhs.back().item.weight * rhs.size();
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);


    // Sorting by weight.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs) -> bool {
                return lhs.back().item.weight < rhs.back().item.weight;
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by sum area.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs) -> bool {
                return lhs.back().item.length * lhs.back().item.width * lhs.size()
                    < rhs.back().item.length * rhs.back().item.width * rhs.size();
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by item area.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs) -> bool {
                unsigned long long valueLHS
                    = static_cast<unsigned long long>(lhs.back().item.length)
                    * static_cast<unsigned long long>(lhs.back().item.width);

                unsigned long long valueRHS
                    = static_cast<unsigned long long>(rhs.back().item.length)
                    * static_cast<unsigned long long>(rhs.back().item.width);

                return valueLHS < valueRHS;
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by sum volume.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs) -> bool {
                unsigned long long valueLHS
                    = static_cast<unsigned long long>(lhs.back().item.length)
                    * static_cast<unsigned long long>(lhs.back().item.width)
                    * static_cast<unsigned long long>(lhs.back().item.height)
                    * static_cast<unsigned long long>(lhs.size());

                unsigned long long valueRHS
                    = static_cast<unsigned long long>(rhs.back().item.length)
                    * static_cast<unsigned long long>(rhs.back().item.width)
                    * static_cast<unsigned long long>(rhs.back().item.height)
                    * static_cast<unsigned long long>(rhs.size());

                return valueLHS < valueRHS;
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

    // Sorting by item volume.
    std::sort(std::begin(groups), std::end(groups),
            [](const  std::vector<ItemProxy>& lhs, const std::vector<ItemProxy>& rhs)  -> bool {
                unsigned long long valueLHS
                    = static_cast<unsigned long long>(lhs.back().item.length)
                    * static_cast<unsigned long long>(lhs.back().item.width)
                    * static_cast<unsigned long long>(lhs.back().item.height);

                unsigned long long valueRHS
                    = static_cast<unsigned long long>(rhs.back().item.length)
                    * static_cast<unsigned long long>(rhs.back().item.width)
                    * static_cast<unsigned long long>(rhs.back().item.height);

                return valueLHS < valueRHS;
            }
        );
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);
    std::reverse(std::begin(groups), std::end(groups));
    UpdateBestAllocation(groups, bins, context, solver, selectedAllocation, bestScore, updated);

}

StandardCompositeSolver::StandardCompositeSolver() {
    memberFillers.push_back(std::shared_ptr<IFiller>(new RoundFiller(true)));
    memberFillers.push_back(std::shared_ptr<IFiller>(new RoundFiller(false)));
    memberFillers.push_back(std::shared_ptr<IFiller>(new XYFiller(true)));
    memberFillers.push_back(std::shared_ptr<IFiller>(new YXFiller(true)));

    const int FILLER_COUNT = memberFillers.size();
    std::vector<int> subset(FILLER_COUNT, 0);
    std::vector<int> finish(FILLER_COUNT, 1);

    memberSolvers.push_back(std::unique_ptr<ISolver>(new StraightSolver));

    do {
        for (int idx = 0; idx < FILLER_COUNT; ++idx) {
            if (subset[idx] == 1) {
                subset[idx] = 0;
            } else {
                subset[idx] = 1;
                break;
            }
        }
        std::vector<std::shared_ptr<IFiller>> fillers;
        for (int idx = 0; idx < FILLER_COUNT; ++idx) {
            if (subset[idx]) {
                fillers.push_back(memberFillers[idx]);
            }
        }
        memberSolvers.push_back(std::unique_ptr<ISolver>(
                new LocallyOptimalFillerAndBinByVolumeChooserSolver(fillers)));
        memberSolvers.push_back(std::unique_ptr<ISolver>(
                new LocallyOptimalFillerIterativeBinByVolumeChooserSolver(fillers)));
        memberSolvers.push_back(std::unique_ptr<ISolver>(
                new LocallyOptimalFillerAndBinByAreaChooserSolver(fillers)));

    } while (subset != finish);
};

std::vector<FilledBinProxy> StandardCompositeSolver::Solve(
        std::vector<std::vector<ItemProxy>> groups, const std::vector<Bin>& bins, const Context& context) {
    std::vector<FilledBinProxy> selectedAllocation;
    esicup::checker::Objective bestScore;
    bool updated = false;
    unsigned idxBestSolver = 0;
    for (int idx = static_cast<int>(memberSolvers.size()) - 1; idx != 0; --idx) {
        if (UpdateBestAllocation(groups, bins, context, memberSolvers[idx],
                                &selectedAllocation, &bestScore, &updated)) {
            idxBestSolver = idx;
        };
    }
    std::cerr << "StandardCompositeSolver::Solve. Optimal Approach: ";
    std::cerr << memberSolvers[idxBestSolver]->GetName() << std::endl;
    return selectedAllocation;
}

std::vector<FilledBinProxy> SortedCompositeSolver::Solve(
        std::vector<std::vector<ItemProxy>> groups, 
        const std::vector<Bin>& bins, const Context& context) {
    unsigned randomState = 0;
    std::srand(randomState);

    std::vector<FilledBinProxy> selectedAllocation;
    Objective bestScore;

    bool updated = false;
    int N_GLOBAL_RUNS = 10;
    for (int idxShuffle = 0; idxShuffle < N_GLOBAL_RUNS; ++idxShuffle) {
        UpdateBestAllocation(groups, bins, context, solver, &selectedAllocation, &bestScore, &updated);
        std::random_shuffle(std::begin(groups), std::end(groups));
    }
    UpdateByApplyingSortings(groups, bins, context, solver, &selectedAllocation, &bestScore, &updated);
    return selectedAllocation;
}

} // namespace algo
} // namespace esicup

