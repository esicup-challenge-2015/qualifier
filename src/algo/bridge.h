/**
 * @file bridge.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Bridges proxy format and the format suitable for computing objective value.
 * The method responsible for conversion is called ConvertToOutputData, the rest of
 * the implemented methods are auxiliary.
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */

#pragma once

#include "proxy.h"
#include "../output.h"

namespace esicup {
namespace algo {

using esicup::common::OutputData;

/**
 * @brief Checks whether a particular bin proxy can be zero-bin. I.e. verifies 
 * a particular bin proxy for constraints related to product counts.
 * 
 * @param context The context to retrieve constraint-related information. More precisely, 
 *                  acceptable counts and corresponding tolerances.
 * @param bin The bin proxy to verify.
 * 
 * @return True if the corresponding requirements are satisfied, and False otherwise.
 */
bool CheckBin(const Context& context, const FilledBinProxy& binProxy);

/**
 * @brief Ensures that the packing fits the requirement for zero/null bin. 
 *      In case the heading bin in the packing does not fit the corresponding constraints,
 *      the last bin is cleared and brought to front, while the first bin is moved backwards.
 * 
 * @param context The context to retrieve constraint-related information. 
 * @param filledBinProxies The vector with current packing.
 */
void AddZeroBin(const Context& context, std::vector<FilledBinProxy>* filledBinProxies);

/**
 * @brief Converts a vector with filled bin proxies to the format suitable to calculate
 * objective value. In the progress, verifies if the requirement about the null bin holds.
 * If not, updates the input vector s.t. the constraint is satisfied.
 * 
 * @param sourceFilledBinProxies The packing to start with when creating output data.
 * @param context The context to retrieve constraint-related information.
 * 
 * @return The OutputData representing packing, that is compatible 
 * with objective calculating routines. 
 */
OutputData ConvertToOutputData(
        const std::vector<FilledBinProxy>& sourceFilledBinProxies, const Context& context);

} // namespace algo
} // namespace esicup
