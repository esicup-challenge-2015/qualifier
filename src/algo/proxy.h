/**
 * @file proxy.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Implements proxies for basic data structures (items, stacks, bins) that simplify
 * algorithm implementations.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "../geometry.h"
#include "../input.h"

#include <limits>

namespace esicup {
namespace algo {

using esicup::common::Context;
using esicup::common::Material;
using esicup::common::Bin;
using esicup::common::Item;
using esicup::common::Bin;
using esicup::common::Context;
using esicup::common::Material;
using esicup::common::InputData;
using esicup::geometry::Point3D;

/**
 * @brief Proxy for an Item with an opportunity to store 
 * related information and data structures. Such organization
 * allows to simplify algorithm implementations.
 */
struct ItemProxy {
public:
    /**
     * @brief Constructor for ItemProxy. Initializes position and hashing for comfortable sortings.
     * 
     * @param itemInit Item for initialization.
     */
    ItemProxy(Item itemInit) : item(itemInit) , hash(0) , position(0) {
        const long long HASH_PRIME = 113;
        if (item.canBeLengthwise) {
            position += 1;
        }
        if (item.canBeWidthwise) {
            position += 2;
        }
        hash = hash * HASH_PRIME + int(position);
        hash = hash * HASH_PRIME + int(item.material);
        hash = hash * HASH_PRIME + item.width;
        hash = hash * HASH_PRIME + item.height;
        hash = hash * HASH_PRIME + item.length;
        hash = hash * HASH_PRIME + item.weight;
    }

    /**
     * @brief Counts the maximal number of objects in stack subject to challenge requirements.
     * 
     * @param context The context to use for computing the numbers.
     * @return The maximal amount of items in a stack according to requirements (13) and (29).
     */
    unsigned GetMaxNumberInStack(const Context& context) const {
        const size_t INFINITY = std::numeric_limits<size_t>::max();
        const double THOUSAND = 1000.;
        unsigned by29requirement = item.material == Material::METAL
            ? INFINITY : (context.maximum_weigth_above_item_kg / item.weight) + 1;
        auto area = static_cast<unsigned long long>(item.width)
            * static_cast<unsigned long long>(item.length);
        unsigned by13requirement = item.material == Material::METAL ? INFINITY :
            context.maximum_density_allowed_kg * area / item.weight / (THOUSAND * THOUSAND);
        if (by13requirement < 1) {
            by13requirement = 1;
        }
        return std::min(by29requirement, by13requirement);
    }

    /**
     * @brief Compares item proxy to another item proxy 
     * according to the hash, position, height, weight, 
     * width, length and material (in this order).
     * 
     * @param rhs The item proxy to compare with.
     * @return True if this item proxy is less than the other, and False otherwise.
     */
    bool IsLess(const ItemProxy& rhs) const {
        if (hash != rhs.hash) {
            return hash < rhs.hash;
        } else if (position != rhs.position) {
            return position < rhs.position;
        } else if (item.height != rhs.item.height) {
            return item.height < rhs.item.height;
        } else if (item.weight != rhs.item.weight) {
            return item.weight < rhs.item.weight;
        } else if (item.width != rhs.item.width) {
            return item.width < rhs.item.width;
        } else if (item.length != rhs.item.length) {
            return item.length < rhs.item.width;
        } else {
            return item.material < rhs.item.material;
        }
    }

    /**
     * @brief Checks if this item proxy is equal to another item proxy.
     * 
     * @param rhs The item proxy to compare with.
     * @return True if item proxies are equal, and False otherwise. 
     */
    bool IsEqual(const ItemProxy& rhs) const {
        return hash == rhs.hash &&
               position == rhs.position &&
               item.height == rhs.item.height &&
               item.weight == rhs.item.weight &&
               item.width == rhs.item.width &&
               item.length == rhs.item.length &&
               item.material == rhs.item.material;
    }

public:
    /// @brief The item funding the proxy.
    Item item;
    /// @brief The proxy hash.
    long long hash;
    /// @brief Encoding for available position. Equals 1 if lengthwise-only, 
    /// 2 if widthwise-only, 3 if both lengthwise and widthwise.
    int position;
    /// @brief Origin of the location (when packed).
    Point3D origin;
    /// @brief Extremity of the location (when packed).
    Point3D extremity;
};

/**
 * @brief A proxy for a stack with items, suitable for 
 * comfortable algorithm implementations.
 */
struct StackProxy {
public:
    /// @brief The vector of item proxies for items in stack.
    std::vector<ItemProxy> items;
    /// @brief Origin of the stack location (when packed).
    Point3D origin;
    /// @brief Extremity of the stack location (when packed).
    Point3D extremity;
};

/**
 * @brief A proxy for a bin (truck), with relevant information
 * and corresponding structures, suitable for comfortable 
 * algorithm implementations.
 */
struct FilledBinProxy {
public:
    /**
     * @brief Calculates statistics about the containers 
     * currently packed/loaded in the truck/bin.
     */
    void CalculateParameters() {
        unsigned long long coveredArea = 0;
        usefulAreaRatio = 0;
        stackVolume = 0;
        usefulVolume = 0;
        usefulVolumeRatio = 0;
        
        itemCount = 0;
        for (const auto& proxyStack: stackProxies) {
            auto currentStackArea
                = static_cast<unsigned long long>(proxyStack.extremity.x - proxyStack.origin.x)
                * static_cast<unsigned long long>(proxyStack.extremity.y - proxyStack.origin.y);
            coveredArea += currentStackArea;
            stackVolume += currentStackArea * static_cast<unsigned long long>(
                    proxyStack.extremity.z - proxyStack.origin.z);
            for (const auto& itemProxy : proxyStack.items) {
                ++itemCount;
                unsigned long long width = itemProxy.item.width;
                unsigned long long length = itemProxy.item.length;
                unsigned long long height = itemProxy.item.height;
                usefulVolume += (width * length * height);
            }
        }
        usefulVolumeRatio = static_cast<double>(usefulVolume) / (static_cast<double>(bin.width)
            * static_cast<double>(bin.height) * static_cast<double>(bin.length));
        usefulAreaRatio = coveredArea / (
                static_cast<double>(bin.width) * static_cast<double>(bin.length));

        const double THOUSAND = 1000.;
        usefulVolume /= (THOUSAND * THOUSAND * THOUSAND);
    }

public:
    /// @brief The bin/truck object where the items are packed.
    Bin bin;
    /// @brief The proxies of stacks packed in the bin.
    std::vector<StackProxy> stackProxies;

    /// @brief The ratio of the area covered by stacks to the area of the truck/bin.
    double usefulAreaRatio;
    /// @brief The ratio of the volume utilized by items to the volume of the truck/bin.
    double usefulVolumeRatio;
    /// @brief The volume of all packed items.
    unsigned long long usefulVolume;
    /// @brief The volume of all stacks.
    unsigned long long stackVolume;
    /// @brief Then number of items packed inside.
    unsigned itemCount;
};

} // namespace algo
} // namespace esicup

