/**
 * @file interfaces.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Defines interfaces for Fillers and Solvers. Fillers represent
 * entities to pack (or up-pack) a particular bin/truck, while Solvers
 * are entities to prepare the entire packing.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 * 
 */
#pragma once

#include "proxy.h"

#include <memory>
#include <string>

namespace esicup {
namespace algo {

/**
 * @brief IFiller is an interface for Filler - an 
 * entity to fill a particular bin (truck).
 */
class IFiller {
public:
    /**
     * @brief FillBin is designed to fill a particular bin using 
     * some of the items available in groups.
     * 
     * @param bin     The bin to fill.
     * @param groups  The items to use for filling.
     * @param context The context to look for contraint management.
     * 
     * @return Bin proxy representing filled bin. Groups are updated 
     *         s.t. items brought to the bin are removed from the group.
     */
    virtual FilledBinProxy FillBin(
            const Bin& bin,
            std::vector<std::vector<ItemProxy>>& groups,
            const Context& context) = 0;
    
    /**
     * @brief Method to get the name of the filler.
     * 
     * @return The name of the filler.
     */
    virtual std::string GetName() const = 0;

    /**
     * @brief Virtual destructor for the IFiller interface.
     */
    virtual ~IFiller() {}
};

/**
 * @brief ISolver is an interface for Solver - an entity
 * to prepare entire bin/truck packing.
 */
class ISolver {
public:
    /**
     * @brief Interface to the method to prepare bin/truck packing.
     * 
     * @param groups   The items to pack, in the form of groups of proxies representing the same item.
     * @param bins     The bins/trucks available to use for packing.
     * @param context  The context with information related to data and constraints.
     * @return The vector containing result - packed bin proxies.
     */
    virtual std::vector<FilledBinProxy> Solve(
            std::vector<std::vector<ItemProxy>> groups,
            const std::vector<Bin>& bins,
            const Context& context) = 0;
    /**
     * @brief Provides the name of the solver.
     * 
     * @return The name of particular solver implementation.
     */
    virtual std::string GetName() const = 0;
    /**
     * @brief Virtual destructor to correctly apply C++ polymorphism.
     */
    virtual ~ISolver() {}
};


} // namespace algo
} // namespace esicup

