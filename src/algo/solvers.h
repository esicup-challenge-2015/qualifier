/**
 * @file solvers.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Specifies various solvers and related API. Solvers create packings
 * in the form of vectors with FilledBinProxy entries.
 * 
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "interfaces.h"
#include "fillers.h"
#include "../objective.h"

namespace esicup {
namespace algo {

using esicup::checker::Objective;

/**
 * @brief Updates the currently most effective allocation by creating 
 * new packing with specified solver and comparing the obtained allocation 
 * to the most effective allocation before the function is called.
 * 
 * @param groups The groups with items to pack.
 * @param bins Available bin opportunities for choosing next bin.
 * @param context The context to manage requirements.
 * @param solver The solver applied for packing.
 * @param selectedAllocation The currently most effective allocation. 
 *          May be updated if the new allocation is better.
 * @param bestScore The currently best objective value. 
 *          May be updated if the new allocation is better.
 * @param updated Whether the allocation has been updated at least once.
 * @return True if the most effective packing and metric are updated,
 *          and False otherwise.
 */
bool UpdateBestAllocation(
        const std::vector<std::vector<ItemProxy>>& groups, 
        const std::vector<Bin>& bins, 
        const Context& context,
        const std::unique_ptr<ISolver>& solver, 
        std::vector<FilledBinProxy>* selectedAllocation,
        Objective* bestScore, 
        bool* updated);

/**
 * @brief Applies various sortings to item groups and calls UpdateBestAllocation on the results.
 * 
 * @param groups The groups with items to pack.
 * @param bins The bin opportunities (types of bins/trucks).
 * @param context The context to manage requirements.
 * @param solver The solver applied for packing.
 * @param selectedAllocation The currently most effective allocation.
 * @param bestScore The currently best objective value.
 * @param updated Whether the allocation has been updated at least once.
 */
void UpdateByApplyingSortings(
        std::vector<std::vector<ItemProxy>> groups,
        const std::vector<Bin>& bins,
        const Context& context,
        const std::unique_ptr<ISolver>& solver,
        std::vector<FilledBinProxy>* selectedAllocation,
        Objective* bestScore,
        bool* updated);

/**
 * @brief Straight solver applies a particular filler (XYFiller)
 * bin-by-bin while all items are yet not packed. 
 */
class StraightSolver : public ISolver {
public:
    /**
     * @brief Constructor for Straight Solver.
     */
    StraightSolver(): filler(new XYFiller(true)) {}

    /**
     * @brief Packs the containers/items to the trucks/bins and
     * creates a packing from scratch.
     * 
     * @param groups The groups to use for packing.
     * @param bins The bins/trucks available to use for packing.
     * @param context The context to ensure that the setting requirements are satisfied.
     * @return The vector with proxies for filled bins (trucks).
     */
    virtual std::vector<FilledBinProxy> Solve(
            std::vector<std::vector<ItemProxy>> groups,
            const std::vector<Bin>& bins,
            const Context& context) override;

    /**
     * @brief Builds the name of the solver. 
     * Also specifies the filler applied.
     * 
     * @return The name of the solver.
     */
    virtual std::string GetName() const override {
        std::string methodName = "StraightSolver. " ;
        std::string fillerName = filler->GetName();
        return methodName + fillerName;
    }

private:
    /// @brief The filler to apply on each particular bin.
    std::unique_ptr<IFiller> filler;
};

/**
 * @brief LocallyOptimalFillerIterativeBinByVolumeChooser Solver 
 * packs new bin in a locally optimal according to volume 
 * ratio fashion, iteratively assigning bin type in circular 
 * fashion and selecting the most effective filler according to 
 * the effective volume utilization metric.
 */
class LocallyOptimalFillerIterativeBinByVolumeChooserSolver : public ISolver {
public:
    /**
     * @brief Constructor. Adds YXFiller, XYFiller and RoundFiller, each with
     * two configurations.
     */
    LocallyOptimalFillerIterativeBinByVolumeChooserSolver() {
        fillers.push_back(std::shared_ptr<IFiller>(new YXFiller(true)));
        fillers.push_back(std::shared_ptr<IFiller>(new XYFiller(true)));
        fillers.push_back(std::shared_ptr<IFiller>(new YXFiller(false)));
        fillers.push_back(std::shared_ptr<IFiller>(new XYFiller(false)));
        fillers.push_back(std::shared_ptr<IFiller>(new RoundFiller(false)));
        fillers.push_back(std::shared_ptr<IFiller>(new RoundFiller(true)));
    }
    /**
     * @brief Constructor. Initializes available fillers according to
     * the specified parameter.
     * 
     * @param fillerInit The vector with fillers to apply for each particular bin.
     */
    LocallyOptimalFillerIterativeBinByVolumeChooserSolver(
            std::vector<std::shared_ptr<IFiller>> fillerInit)
        : fillers(fillerInit) {}
    /**
     * @brief Initialization method. Assigns the vector of fillers to the specified.
     * 
     * @param fillerInit The vector of fillers to apply for each particular bin.
     */
    void Initialize(std::vector<std::shared_ptr<IFiller>> fillerInit) {
        fillers = fillerInit;
    }
    /**
     * @brief Builds name for the solver. Also specifies applied fillers.
     * 
     * @return Description (name) of the solver.
     */
    virtual std::string GetName() const override {
        std::string methodName = "Solver: Locally Optimal by Volume. Sequential Bin Update.\n";
        std::string optionName = "Filler Subset: ";
        for (const auto& entry: fillers) {
            optionName = optionName + entry->GetName() + " ";
        }
        return methodName + optionName;
    }

    /**
     * @brief Packs the containers/items to the trucks/bins and
     * creates a packing from scratch. Forms new bin/truck in a locally optimal according
     * to effective volume ratio fashion, iteratively assigning bin type in 
     * circular mode and selecting the most effective filler according
     * to the effective volume utilization metric.
     * 
     * @param groups The groups to use for packing.
     * @param bins The bins/trucks available to use for packing.
     * @param context The context to ensure that the setting requirements are satisfied.
     * @return The vector with proxies for filled bins (trucks).
     */
    virtual std::vector<FilledBinProxy> Solve(
            std::vector<std::vector<ItemProxy>> groups,
            const std::vector<Bin>& bins,
            const Context& context) override;
private:
    /// @brief The vector with fillers to apply when packing a bin.
    std::vector<std::shared_ptr<IFiller>> fillers;
};

/**
 * @brief LocallyOptimalFillerAndBinByVolumeChooser Solver packs new 
 * bin in a locally optimal according to the volume ratio fashion, 
 * iteratively selecting the most effective filler and bin type according 
 * to the effective volume utilization metric.
 */
class LocallyOptimalFillerAndBinByVolumeChooserSolver : public ISolver {
public:
    /**
     * @brief Constructor. Adds YXFiller, XYFiller and RoundFiller, each with
     * two configurations.
     */
    LocallyOptimalFillerAndBinByVolumeChooserSolver() {
        fillers.push_back(std::shared_ptr<IFiller>(new YXFiller(true)));
        fillers.push_back(std::shared_ptr<IFiller>(new XYFiller(true)));
        fillers.push_back(std::shared_ptr<IFiller>(new YXFiller(false)));
        fillers.push_back(std::shared_ptr<IFiller>(new XYFiller(false)));
        fillers.push_back(std::shared_ptr<IFiller>(new RoundFiller(false)));
        fillers.push_back(std::shared_ptr<IFiller>(new RoundFiller(true)));
    }
    /** 
     * @brief Constructor. Initializes available fillers according to
     * the specified parameter.
     * 
     * @param fillerInit The vector with fillers to apply for each particular bin.
     */
    LocallyOptimalFillerAndBinByVolumeChooserSolver(
            std::vector<std::shared_ptr<IFiller>> fillerInit) : fillers(fillerInit) {}
    /**
     * @brief Initialization method. Assigns the vector of fillers to the specified.
     * 
     * @param fillerInit The vector of fillers to apply for each particular bin.
     */
    void Initialize(std::vector<std::shared_ptr<IFiller>> fillerInit) {
        fillers = fillerInit;
    }
    /**
     * @brief Builds name for the solver. Also specifies available fillers.
     * 
     * @return Description (name) of the solver.
     */
    virtual std::string GetName() const override {
        std::string methodName = "Solver: Locally Optimal by Volume. Optimal Bin Selection.\n";
        std::string optionName = "Filler Subset: ";
        for (const auto& entry: fillers) {
            optionName = optionName + entry->GetName() + " ";
        }
        return methodName + optionName;
    }
   /**
     * @brief Packs the containers/items to the trucks/bins and
     * creates a packing from scratch. Forms new bin in a locally optimal according
     * to effective volume ratio fashion, iteratively selecting the most 
     * effective filler and bin type according to the effective volume utilization metric.
     * 
     * @param groups The groups to use for packing.
     * @param bins The bins/trucks available to use for packing.
     * @param context The context to ensure that the setting requirements are satisfied.
     * @return The vector with proxies for filled bins (trucks).
     */
    virtual std::vector<FilledBinProxy> Solve(
            std::vector<std::vector<ItemProxy>> groups,
            const std::vector<Bin>& bins,
            const Context& context) override;
private:
    /// @brief The vector with fillers to apply when packing a bin.
    std::vector<std::shared_ptr<IFiller>> fillers;
};

/**
 * @brief LocallyOptimalFillerAndBinByAreaChooser Solver packs new 
 * bin in a locally optimal according to the area ratio fashion, 
 * iteratively selecting the most effective filler and bin type according 
 * to the effective area utilization metric.
 */
class LocallyOptimalFillerAndBinByAreaChooserSolver : public ISolver {
public:
    /**
    * @brief Constructor. Adds YXFiller, XYFiller and RoundFiller, each with
    * two configurations.
    */
    LocallyOptimalFillerAndBinByAreaChooserSolver() {
        fillers.push_back(std::shared_ptr<IFiller>(new YXFiller(true)));
        fillers.push_back(std::shared_ptr<IFiller>(new XYFiller(true)));
        fillers.push_back(std::shared_ptr<IFiller>(new YXFiller(false)));
        fillers.push_back(std::shared_ptr<IFiller>(new XYFiller(false)));
        fillers.push_back(std::shared_ptr<IFiller>(new RoundFiller(false)));
        fillers.push_back(std::shared_ptr<IFiller>(new RoundFiller(true)));
    }
    /** 
     * @brief Constructor. Initializes available fillers according to
     * the specified parameter.
     * 
     * @param fillerInit The vector with fillers to apply for each particular bin.
     */
    LocallyOptimalFillerAndBinByAreaChooserSolver(
        std::vector<std::shared_ptr<IFiller>> fillerInit)
        : fillers(fillerInit) {}
    /**
     * @brief Initialization method. Assigns the vector of fillers to the specified.
     * 
     * @param fillerInit The vector of fillers to apply for each particular bin.
     */
    void Initialize(std::vector<std::shared_ptr<IFiller>> fillerInit) {
        fillers = fillerInit;
    }
    /**
     * @brief Builds name for the solver. Also specifies available fillers.
     * 
     * @return Description (name) of the solver.
     */
    virtual std::string GetName() const override {
        std::string methodName = "Solver: Locally Optimal by Area. Optimal Bin Selection.\n";
        std::string optionName = "Filler Subset: ";
        for (const auto& entry: fillers) {
            optionName = optionName + entry->GetName() + " ";
        }
        return methodName + optionName;
    }
   /**
     * @brief Packs the containers/items to the trucks/bins and
     * creates a packing from scratch. Forms new bin in a locally optimal according
     * to effective area ratio fashion, iteratively selecting the most 
     * effective filler and bin type according to the effective area utilization metric.
     * 
     * @param groups The groups to use for packing.
     * @param bins The bins/trucks available to use for packing.
     * @param context The context to ensure that the setting requirements are satisfied.
     * @return The vector with proxies for filled bins (trucks).
     */
    virtual std::vector<FilledBinProxy> Solve(
        std::vector<std::vector<ItemProxy>> groups,
        const std::vector<Bin>& bins,
        const Context& context) override;

private:
    /// @brief The vector with fillers to apply when packing a bin.
    std::vector<std::shared_ptr<IFiller>> fillers;
};

/**
 * @brief Standard composite solver. Creates the most effective packing
 * by creating multiple with different solvers and selecting the output
 * according to objective metric.
 */
class StandardCompositeSolver : public ISolver {
public:
    /**
     * @brief Constructor. Fills memberSolvers and memberFillers.
     * Essentially, first specifies the set of fillers. Then, for 
     * each subset of fillers, and each solver class, creates a
     * corresponding solver instance.     * 
     */
    StandardCompositeSolver();
    /**
     * @brief Packs the containers/items to the trucks/bins and
     * creates a packing from scratch. Applies all options from memberSolvers
     * and selects the most effective according to objective metric.
     * 
     * @param groups The groups to use for packing.
     * @param bins The bins/trucks available to use for packing.
     * @param context The context to ensure that the setting requirements are satisfied.
     * @return The vector with proxies for filled bins (trucks).
     */
    virtual std::vector<FilledBinProxy> Solve(
            std::vector<std::vector<ItemProxy>> groups,
            const std::vector<Bin>& bins,
            const Context& context) override;
    /**
     * @brief Builds solver name.
     * 
     * @return The name of the solver.
     */
    virtual std::string GetName() const override {
        return "Solver: Standard Composite.";
    }

private:
    /// @brief Solvers to apply for selecting the most effective packing.
    std::vector<std::unique_ptr<ISolver>> memberSolvers;
    /// @brief Fillers to build solvers on top.
    std::vector<std::shared_ptr<IFiller>> memberFillers;
};

/**
 * @brief Sorted composite solver. Applies different sortings to the 
 * item groups, and also several random shufflings, and calls the
 * standard composite solver on top. The most effective packing is 
 * selected according to the objective metric. 
 */
class SortedCompositeSolver : public ISolver {
public:
    /**
     * @brief Constructor for SortedCompositeSolver. Initializes
     * the solver pointer to reference StandardCompositeSolver.
     */
    SortedCompositeSolver() : solver(new StandardCompositeSolver) {}
    /**
     * @brief Packs the containers/items to the trucks/bins and
     * creates a packing from scratch. Applies different sortings to the 
     * item groups, and also several random shufflings, and calls the
     * standard composite solver on top. The most effective packing is 
     * selected according to the objective metric.
     * 
     * @param groups The groups to use for packing.
     * @param bins The bins/trucks available to use for packing.
     * @param context The context to ensure that the setting requirements are satisfied.
     * @return The vector with proxies for filled bins (trucks).
     */
    virtual std::vector<FilledBinProxy> Solve(
            std::vector<std::vector<ItemProxy>> groups,
            const std::vector<Bin>& bins,
            const Context& context) override;
    /**
     * @brief Builds solver name.
     * 
     * @return The name of the solver.
     */
    virtual std::string GetName() const override {
        return "Solver: Sorted Composite.";
    }

private:
    /// @brief The pointer to the solver to apply after each sorting is performed.
    std::unique_ptr<ISolver> solver;
};

} // namespace algo
} // namespace esicup