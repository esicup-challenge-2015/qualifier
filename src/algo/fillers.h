/**
 * @file fillers.h
 * @author Aliaksandr Nekrashevich (aliaksandr.nekrashevich@queensu.ca)
 * @brief Specifies Fillers and corresponding API.
 * @copyright (c) Belarusian State University, 2015
 * @copyright (c) Smith School of Business, 2024
 */
#pragma once

#include "interfaces.h"

namespace esicup {
namespace algo {

using esicup::geometry::Point3D;
using esicup::geometry::PosedRectangle;
using esicup::geometry::Point2D;

/**
 * @brief Filler to pack bins row-by-row.
 */
class XYFiller : public IFiller {
public:
    /**
     * @brief Constructor for XYFiller. 
     * 
     * @param initCanSkip Initialization for skip parameter.
     */
    XYFiller(bool initCanSkip) : canSkip(initCanSkip) {}
    /**
     * @brief Fills the bin row-by-row.
     * 
     * @param bin      The bin to fill.
     * @param groups   The groups of items to pack into the bin.
     * @param context  The context to account for the constraints.
     * @return FilledBinProxy 
     */
    virtual FilledBinProxy FillBin(
            const Bin& bin,
            std::vector<std::vector<ItemProxy> >& groups,
            const Context& context) override;
    /**
     * @brief Returns the filler name.
     * 
     * @return The name of the filler.
     */
    virtual std::string GetName() const override;

private:
    /// @brief Whether to skip the current item type and go to next group is allowed.
    /// If not, the filler stops proceeding and returns the currently achieved packing.
    bool canSkip;
};

/**
 * @brief Packs the bins (trucks) column by column.
 */
class YXFiller : public IFiller {
public:
    /**
     * @brief Constructor fo YXFiller.
     * 
     * @param initCanSkip Initializer for canSkip member.
     *    Stands for opportunity to continue with another item
     *    if the current item cannot get packed.
     */
    YXFiller(bool initCanSkip) : canSkip(initCanSkip) {}
    /**
     * @brief Fills the bin with the next item.
     * 
     * @param bin     The bin to fill.
     * @param groups  The groups with items for filling.
     * @param context The context to account for the constraints.
     * 
     * @return The proxy for the filled bin. Groups are updated accordingly.
     */
    virtual FilledBinProxy FillBin(
            const Bin& bin,
            std::vector< std::vector<ItemProxy> >& groups,
            const Context& context) override;
    
    /**
     * @brief Builds the name for the YX Filler.
     * 
     * @return The name of the filler.
     */
    virtual std::string GetName() const override;

private:
    /// @brief If true, packing continues if the current item could not fit.
    /// Otherwise, the packing stops at such event.
    bool canSkip;
};

/**
 * @brief Filler to fill a truck from center circularly.
 */
class RoundFiller : public IFiller {
public:
    /**
     * @brief Constructor for Round Filler.
     * 
     * @param adjustToBinCenter Specifies whether to select origin at bin center.
     */
    RoundFiller(bool adjustToBinCenter) : centralize(adjustToBinCenter) {}
    /**
     * @brief Fills the bin with the next item.
     * 
     * @param bin     The bin to fill.
     * @param groups  The groups with items for filling.
     * @param context The context to account for the constraints.
     * 
     * @return The proxy for the filled bin. Groups are updated accordingly.
     */
    virtual FilledBinProxy FillBin(
            const Bin& bin,
            std::vector<std::vector<ItemProxy> >& groups,
            const Context& context) override;
    /**
     * @brief Builds the name for the YX Filler.
     * 
     * @return The name of the filler.
     */
    virtual std::string GetName() const override;

private:
    /// @brief Center to start circular packing from.
    Point2D center;
    /// @brief Whether to bring the center to the bin center from origin.
    bool centralize;
};

/**
 * @brief Verifies if a rectangle can be located together with a vector with already allocated
 * rectangles. Essentially, yes iff no intersections between the new rectangle and existing
 * rectangles.
 * 
 * @param fixedRectangles The set of currently allocated rectangle.
 * @param currentRectangle The new rectangle under consideration 
 *                          to get added to the set of already fixed.
 * 
 * @return True if the currentRectangle can be added to fixedRectangles without 
 * violations, and False otherwise.
 */
bool IfCanPutRectangle(
    const std::vector<PosedRectangle>& fixedRectangles, PosedRectangle& currentRectangle);

/**
 * @brief Tries to put the next item (itemProxy) to some of the currently existing stacks. If there is such 
 * an opportunity, updates the FilledBinProxy s.t. the item is assigned there.
 * 
 * @param itemProxy The item to try to pack into some stack at FilledBin.
 * @param context The context to check for requirements.
 * @param bin The bin where the item is tried to get added.
 * 
 * @return True if there is an opportunity (and then the item 
 *          is assigned somewhere), and False otherwise.
 */
bool TryPutToAllStacks(const ItemProxy& itemProxy, const Context& context, FilledBinProxy* bin);

/**
 * @brief Counts how many items can be put to the bin.
 * 
 * @param group The group with items of the same shape and structure to try allocating to the bin.
 * @param bin The bin for allocating the items.
 * @param context The context to look for requirements and setting.
 * 
 * @return The amount, how many items would fit the bin.
 */
unsigned CountHowManyCanPut(
    const std::vector<ItemProxy>& group, const Bin& bin, const Context& context);

/**
 * @brief Specifies length and width into the variables provided by modifiable references. 
 * Since some of the items are lengthwise only, while others widthwise only, length and 
 * width require explicit specification.
 * 
 * @param itemProxy The item to look into.
 * @param bin The bin where this item is considered for packing.
 * @param length The length of the item (to be assigned).
 * @param width The width of the item (to be assigned).
 */
void SpecifyPosition(
    const ItemProxy& itemProxy, const Bin& bin, unsigned& length, unsigned& width); 

/**
 * @brief Checks whether the itemProxy can be added to 
 * the stack denoted by stackProxy according to the 
 * specified search for a potential place. If yes, the 
 * resulting stack proxy is written to resultStackProxy.
 * 
 * @param itemProxy The item to try adding to the bin.
 * @param binProxy The bin to try adding the item at.
 * @param context The context to look for constraints and requirements.
 * @param stackProxy The stack to try adding the item at.
 * @param resultStackProxy If there is a way to add the item, 
 *              here is where the result after adding is stored.
 * 
 * @return True if the function has found a way to add the item, and False otherwise.
 */
bool IfCanAdd(const ItemProxy& itemProxy, const FilledBinProxy& binProxy, const Context& context,
        const StackProxy& stackProxy, StackProxy* resultStackProxy);

} // namespace algo
} // namespace esicup