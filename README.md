# ESICUP Challenge 2015 - Submission For Qualification Stage

## Launching The Solution

Working Environment: Ubunut Linux 22.04 LTS

1. Retrieving relevant submodules: `git submodule update --init --recursive`
2. Getting to the folder with sources: `cd src`
3. Compilation: `make`
4. Launch for Instances A: `./main -p ../data/instancesA`
5. Launch for Instances B: `./main -p ../data/instancesB`
6. Launch for Instances X: `./main -p ../data/instancesX`

## Repository Organization

- `method/method.pdf` - Description of the implemented approach.
- `method/performance.pdf` - Performance table on the qualification set of instances (instancesA).
- `src` - Source code of the qualification round solution.
- `documentation/api.pdf` - Source code documentation, generated with Doxygen.
- `data` - Submodule with data and problem description.

## Documentation With Doxygen

1. Start in repository root.
2. Call Doxygen: `doxygen`.
3. Navigate to `docs/latex`.
4. Call `make`.
5. The result is located in `refman.pdf`.

# Acknowledgement

 The original version of the code was implemented by multiple team members from Belarusian SU. The authorship in the source code file only shows that the corresponding team member decided to mention the authorship - however, there could have been other source code authors. Please see the abstract in the ESICUP meeting for the complete set of team members.
